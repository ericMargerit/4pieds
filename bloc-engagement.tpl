<section id="engagement">
  <span class="span-title">4pieds s'engage</span>
  <ul>
    <li>
      <span class="sprite-engagement sprite-engagement-truck"></span>
      LIVRAISON À DOMICILE
    </li>

    <li>
      <span class="sprite-engagement sprite-engagement-clock"></span>
      SATISFAIT OU REMBOURSÉ*
    </li>

    <li>
      <span class="sprite-engagement sprite-engagement-delivery"></span>
      LIVRAISON & retour gratuits
    </li>

    <li>
      <span class="sprite-engagement sprite-engagement-card"></span>
      PAIEMENT SÉCURISÉ
    </li>

    <li>
      <span class="sprite-engagement sprite-engagement-sample"></span>
      ÉCHANTILLONS SUR DEMANDE
    </li>

    <li>
      <span class="sprite-engagement sprite-engagement-operator"></span>
      DES EXPERTS À VOTRE SERVICE
    </li>
  </ul>
  <div class="clearfix"></div>
  <span class="message_cgv">* Voir nos conditions générales de vente</span>
</section>