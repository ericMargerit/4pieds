{if $smarty.get.page == 1}
  <section id="order-cart">

    <div id="order-list">

      <span class="span-title">Mon Panier</span>

      <p class="error-message error-message-no-bg">
        <span class="sprite-info-message"></span>Votre commande se compose de produit(s) fourni(s) par différents fabricants. La livraison de celle-ci sera effectuée en plusieurs fois.
      </p>

      <div class="only-large" id="order-table-head">
        <span>prix total ttc</span>
        <span>quantité</span>
        <span>prix unitaire ttc</span>
        <div class="clearfix"></div>
      </div>

      <ul id="order-list-products">
        <li>
          <div class="order-product-img">
            <img src="{$img_dir}compressed/illu-chaise-big.jpg" />
          </div>

          <div class="order-product-infos-container">
            <div class="order-product-infos">
              <span class="order-product-name">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE ÉPOQUE</span>

              <div class="order-product-personalize">
                <span class="product-select-color-style product-select-color1"></span> Polycarbonate rouge
              </div>
              <p class="order-product-details">
                Hauteur avec dossier : 97 cm<br/>
                Largeur : 52.5 cm<br/>
                Poids : 5Kg
              </p>
              <div class="order-product-tags">
                <span class="product-tag en-stock">en stock</span>
              </div>
            </div>
            <div class="order-product-price">
              <span class="no-large">PRIX Unitaire ttc :</span>
              <span class="price-old">149<sup>€00</sup></span>
              <span class="price-new">119<sup>€00</sup></span>
            </div>

            <form class="order-product-quantity">
              <span class="no-large">quantité</span>
              <select class="quantity-selector">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option class="more">+</option>
              </select>
            </form>

            <div class="order-product-price-final">
              <span class="no-large">PRIX total ttc :</span>
              <span class="price">149<sup>€00</sup></span>
            </div>

            <div class="order-product-trash">
              <span class="icon-trash"></span>
            </div>
          </div>

          <div class="clearfix"></div>

        </li>

        <li>
          <div class="order-product-img">
            <img src="{$img_dir}compressed/illu-chaise-big.jpg" />
          </div>

          <div class="order-product-infos-container">
            <div class="order-product-infos">
              <span class="order-product-name">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE ÉPOQUE</span>

              <div class="order-product-personalize">
                <span class="product-select-color-style product-select-color1"></span> Polycarbonate rouge
              </div>

              <div class="order-product-personalize">
                <span class="product-select-color-style product-select-color3"></span> Polycarbonate rouge
              </div>
              <p class="order-product-details">
                Hauteur avec dossier : 97 cm<br/>
                Largeur : 52.5 cm<br/>
                Poids : 5Kg
              </p>
              <div class="order-product-tags">
                <span class="product-tag en-stock">en stock</span>
              </div>
            </div>
            <div class="order-product-price">
              <span class="no-large">PRIX Unitaire ttc :</span>
              <span class="price-old">149<sup>€00</sup></span>
              <span class="price-new">119<sup>€00</sup></span>
            </div>

            <form class="order-product-quantity">
              <span class="no-large">quantité</span>
              <select class="quantity-selector">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option class="more">+</option>
              </select>
            </form>

            <div class="order-product-price-final">
              <span class="no-large">PRIX total ttc :</span>
              <span class="price">149<sup>€00</sup></span>
            </div>


            <div class="order-product-trash">
              <span class="icon-trash"></span>
            </div>

          </div>

          <div class="clearfix"></div>

        </li>

      </ul>

    </div>

    {include file="$tpl_dir./cart-total-column.tpl"}
  </section>
{/if}

{if $smarty.get.page == 2}
  <section id="registration">

    <form id="registration-form">

      <span class="span-title">informations personnelles</span>
      <fieldset>
        <span class="registration-radio"><input type="radio" name="id_gender" id="id_gender"></span> <label for="id_gender">M.</label>
        <span class="registration-radio"><input type="radio" name="id_gender" id="id_gender2"></span> <label for="id_gender2">Mme</label>
        <span class="registration-radio"><input type="radio" name="id_gender" id="id_gender3"></span> <label for="id_gender3">Mlle</label>

        <input type="text" name="" placeholder="Prénom" value="" />
        <input type="text" name="" placeholder="Nom" value="" />
        <input type="text" name="" placeholder="Email" value="" />
        <input type="text" name="" placeholder="Mot de passe actuel" value="" />
        <input type="text" name="" placeholder="Nouveau mot de passe" value="" />
        <input type="text" name="" placeholder="Confirmation mot de passe" value="" />

        <div id="registration-select-container">
          <select name="">
            <option>1</option>
            <option>2</option>
            <option>3</option>
          </select>

          <select name="">
            <option>Janvier</option>
            <option>Février</option>
            <option>Mars</option>
          </select>

          <select name="">
            <option>1978</option>
            <option>1979</option>
            <option>1980</option>
          </select>
        </div>

        <span class="registration-checkbox"><input type="checkbox" name="" id="registration-newsletter-optin-checkbox" /></span> <label for="registration-newsletter-optin-checkbox">Découvrir en avant-première les nouveautés en m’inscrivant à la newsletter 4pieds</label>
        <span class="registration-checkbox"><input type="checkbox" name="" id="registration-offer-optin-checkbox"/></span> <label for="registration-offer-optin-checkbox">Profiter des offres partenaires 4pieds</label>

      </fieldset>

      <span class="span-title">adresse de livraison</span>

      <fieldset>

        <input type="text" name="" placeholder="Prénom" value="" />
        <input type="text" name="" placeholder="Nom" value="" />
        <input type="text" name="" placeholder="Adresse" value="" />
        <input type="text" name="" placeholder="Complément d’adresse" value="" />
        <input type="text" name="" placeholder="Ville" value="" />
        <input type="text" name="" placeholder="Code postal" value="" />

      </fieldset>

      <span class="span-title">adresse de facturation</span>

      <fieldset>
        <span class="registration-checkbox registration-delivery-address checked"><input type="checkbox" checked id="registration-delivery-checkbox" name="" /></span> <label for="registration-delivery-checkbox">adresse identique à l’adresse de livraison</label>
        <p id="registration-delivery-address">
          <input type="text" name="" placeholder="Prénom" value="" />
          <input type="text" name="" placeholder="Nom" value="" />
          <input type="text" name="" placeholder="Adresse" value="" />
          <input type="text" name="" placeholder="Complément d’adresse" value="" />
          <input type="text" name="" placeholder="Ville" value="" />
          <input type="text" name="" placeholder="Code postal" value="" />
        </p>
      </fieldset>

      <input type="submit" value="valider mes informations" />

    </form>

    {include file="$tpl_dir./cart-total-column.tpl"}

  </section>
{/if}

{if $smarty.get.page == 3}
  <section id="my-addresses">

    <div id="my-addresses-left">

      <span class="span-title">adresse de livraison</span>

      <ul id="my-addresses-list">
        <li>
          <span class="my-addresses-radio"><input type="radio" name="my-addresses-choice" id="my-addresses-choice"></span>
          <label class="address" for="my-addresses-choice">
            <strong>Benedict Cumberbatch</strong>
            13 Place des Lices - 35000 - RENNES
            <br/>
            France
          </label>

          <div class="address-actions">
            <a href="#"> <span class="icon-edit"></span> </a>
            <a href="#"> <span class="icon-trash"></span> </a>
          </div>

          <div class="clearfix"></div>
        </li>

        <li>
          <span class="my-addresses-radio"><input type="radio" name="my-addresses-choice" id="my-addresses-choice2"></span>
          <label class="address" for="my-addresses-choice2">
            <strong>Benedict Cumberbatch</strong>
            13 Place des Lices - 35000 - RENNES
            <br/>
            France
          </label>

          <div class="address-actions">
            <a href="#"> <span class="icon-edit"></span> </a>
            <a href="#"> <span class="icon-trash"></span> </a>
          </div>

          <div class="clearfix"></div>
        </li>

      </ul>

      <form id="my-address-add">

        <span class="title-dropdown"> <span class="icon-right_arrow"></span> ajouter une nouvelle adresse </span>

        <fieldset class="content-dropdown">
          <input type="text" placeholder="Nom" />
          <input type="text" placeholder="Prénom" />
          <input type="text" placeholder="Adresse" />
          <input type="text" placeholder="Complément d'Adresse" />
          <input type="text" placeholder="Ville" />
          <input type="text" placeholder="Code Postal" />

          <input type="submit" value="Ajouter l’adresse"/>

        </fieldset>

      </form>

      <span class="span-title">adresse de facturation</span>

      <span class="my-address-invoice-checkbox my-address-invoice-address checked"><input type="checkbox" checked id="my-address-invoice-checkbox" name="" /></span> <label for="my-address-invoice-checkbox">adresse identique à l’adresse de livraison</label>
      <form id="my-address-invoice-add">
        <fieldset id="my-address-invoice-address">
          <input type="text" name="" placeholder="Prénom" value="" />
          <input type="text" name="" placeholder="Nom" value="" />
          <input type="text" name="" placeholder="Adresse" value="" />
          <input type="text" name="" placeholder="Complément d’adresse" value="" />
          <input type="text" name="" placeholder="Ville" value="" />
          <input type="text" name="" placeholder="Code postal" value="" />
          <input type="submit" value="Valider l'adresse"/>
        </fieldset>
      </form>

    </div>

    {include file="$tpl_dir./cart-total-column.tpl"}

  </section>
{/if}

{if $smarty.get.page == 4}
  <section id="delivery">

    <form id="delivery-form">
      <ul>
        <li>
          <span class="delivery-radio"><input type="radio" name="delivery-choice" id="delivery-choice"></span>
          <label for="delivery-choice">
            <span class="delivery-choice-label"> retrait en magasin </span>
            <span class="price">GRATUIT</span>
            <small>Délai : + 1 à 2 semaines de transport</small>
          </label>
          <p class="delivery-more-infos">
            <select class="customSelect">
              <option>Choisissez votre magasin</option>
              <option>Paris</option>
              <option>Rennes</option>
            </select>
            Le transporteur Guisnel distribution, spécialisé dans le transport de mobilier, vous livre du lundi au vendredi de 9h à 18h avec prise de rendez-vous sur un créneau de deux heures. Votre commande est livrée et déballée dans la pièce de votre choix et à l'étage gratuitement si nécessaire (valeur de l'option "étage" 50€ TTC)...
          </p>
        </li>
        <li>
          <span class="delivery-radio"><input type="radio" name="delivery-choice" id="delivery-choice2"></span>
          <label for="delivery-choice2">
            <span class="delivery-choice-label">livraison expert sur rendez-vous</span>
            <span class="price">154<sup>€95</sup> ttc</span>
            <small>Délai : +7 à 10 jours de transport</small>
          </label>
          <p class="delivery-more-infos">
            Le transporteur Guisnel distribution, spécialisé dans le transport de mobilier, vous livre du lundi au vendredi de 9h à 18h avec prise de rendez-vous sur un créneau de deux heures. Votre commande est livrée et déballée dans la pièce de votre choix et à l'étage gratuitement si nécessaire (valeur de l'option "étage" 50€ TTC)...
          </p>
        </li>
        <li>
          <span class="delivery-radio"><input type="radio" name="delivery-choice" id="delivery-choice3"></span>
          <label for="delivery-choice3">
            <span class="delivery-choice-label"> livraison sur rendez-vous avec montage </span>
            <span class="price">204<sup>€95</sup> ttc</span>
            <small>Délai : +7 à 10 jours de transport</small>
          </label>
          <p class="delivery-more-infos">
            Le transporteur Guisnel distribution, spécialisé dans le transport de mobilier, vous livre du lundi au vendredi de 9h à 18h avec prise de rendez-vous sur un créneau de deux heures. Votre commande est livrée et déballée dans la pièce de votre choix et à l'étage gratuitement si nécessaire (valeur de l'option "étage" 50€ TTC)...
          </p>
        </li>
      </ul>
    </form>

    {include file="$tpl_dir./cart-total-column.tpl"}

  </section>
{/if}

{if $smarty.get.page == 5}
  <section id="payment">

    <form id="payment-form">

      <div>
        <span class="payment-checkbox">
          <input type="checkbox" id="payment-checkbox" name="" />
        </span>
        <label class="payment-label" for="payment-checkbox">J'accepte les Conditions Générales de Vente et je suis informé de mon droit de rétraction</label>
      </div>


      <div class="span-title">Votre mode de paiement</div>

      <ul>
        <li>
          <span class="payment-radio"><input type="radio" name="payment-choice" id="payment-choice"></span>
          <label for="payment-choice">
            <span class="sprite-cb"></span> carte bancaire
          </label>
          <p class="payment-more-infos">
            Le transporteur Guisnel distribution, spécialisé dans le transport de mobilier, vous livre du lundi au vendredi de 9h à 18h avec prise de rendez-vous sur un créneau de deux heures. Votre commande est livrée et déballée dans la pièce de votre choix et à l'étage gratuitement si nécessaire (valeur de l'option "étage" 50€ TTC)...
            <br/><a href="#"> <span class="icon-validation"></span> payer par carte bancaire</a>
          </p>
        </li>
        <li>
          <span class="payment-radio"><input type="radio" name="payment-choice" id="payment-choice2"></span>
          <label for="payment-choice2">
            <span class="sprite-cb"></span> 3x sans frais en carte bancaire
          </label>
          <p class="payment-more-infos">
            Le transporteur Guisnel distribution, spécialisé dans le transport de mobilier, vous livre du lundi au vendredi de 9h à 18h avec prise de rendez-vous sur un créneau de deux heures. Votre commande est livrée et déballée dans la pièce de votre choix et à l'étage gratuitement si nécessaire (valeur de l'option "étage" 50€ TTC)...
            <br/><a href="#"> <span class="icon-validation"></span> payer en 3x sans frais</a>
          </p>
        </li>
        <li>
          <span class="payment-radio"><input type="radio" name="payment-choice" id="payment-choice3"></span>
          <label for="payment-choice3">
            <span class="sprite-paypal"></span>paypal
          </label>
          <p class="payment-more-infos">
            Sur 4-pieds.com, payez avec votre compte PayPal en inscrivant simplement votre adresse email et votre mot de passe PayPal. Ce type de paiement vous permet de ne pas communiquer vos coordonnées bancaires.
            <br/><a href="#"> <span class="icon-validation"></span> payer sur paypal</a>
          </p>
        </li>
        <li>
          <span class="payment-radio"><input type="radio" name="payment-choice" id="payment-choice4"></span>
          <label for="payment-choice4">
            <span class="icon-cheque"></span> chèque
          </label>
          <p class="payment-more-infos">
            Sur 4-pieds.com, payez avec votre compte PayPal en inscrivant simplement votre adresse email et votre mot de passe PayPal. Ce type de paiement vous permet de ne pas communiquer vos coordonnées bancaires.
            <br/><a href="#"> <span class="icon-validation"></span> payer par chèque</a>
          </p>
        </li>
        <li>
          <span class="payment-radio"><input type="radio" name="payment-choice" id="payment-choice5"></span>
          <label for="payment-choice5">
            <span class="icon-virement"></span> virement
          </label>
          <p class="payment-more-infos">
            Sur 4-pieds.com, payez avec votre compte PayPal en inscrivant simplement votre adresse email et votre mot de passe PayPal. Ce type de paiement vous permet de ne pas communiquer vos coordonnées bancaires.
            <br/><a href="#"> <span class="icon-validation"></span> payer par virement bancaire</a>
          </p>
        </li>
      </ul>
    </form>

    {include file="$tpl_dir./cart-total-column.tpl"}

  </section>
{/if}

{if $smarty.get.page == 6}
  <section id="order-completed">

    <span class="span-title">félicitations, votre commande a été effectuée avec succès !</span>
    <span class="span-subtitle">et maintenant épatez vos amis !</span>

    <ul id="order-completed-list-products">
      <li class="order-completed-product">
        <div class="order-completed-product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
          <ul>
            <li class="active"> <a href="#"><span class="sprite-order-completed-facebook"></span></a> </li>
            <li> <a href="#"><span class="sprite-order-completed-twitter"></span></a> </li>
            <li> <a href="#"><span class="sprite-order-completed-mail"></span></a> </li>
          </ul>
          <form>
            <input type="text" value="" placeholder="email destinataire"/>
            <textarea>Je viens de faire un achat sur 4pieds.com : Table extensible en céramique BAKOU</textarea>
            <button type="submit">
              <span class="sprite-product-share"></span>
              <span>partager</span>
            </button>
          </form>
        </div>
      </li>

      <li class="order-completed-product">
        <div class="order-completed-product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
          <ul>
            <li class="active"> <a href="#"><span class="sprite-order-completed-facebook"></span></a> </li>
            <li> <a href="#"><span class="sprite-order-completed-twitter"></span></a> </li>
            <li> <a href="#"><span class="sprite-order-completed-mail"></span></a> </li>
          </ul>
          <form>
            <input type="text" value="" placeholder="email destinataire"/>
            <textarea>Je viens de faire un achat sur 4pieds.com : Table extensible en céramique BAKOU</textarea>
            <button type="submit">
              <span class="sprite-product-share"></span>
              <span>partager</span>
            </button>
          </form>
        </div>
      </li>

    </ul>

    <div class="product-also-seen">

      <span class="span-title">Nos Clients ont également regardé</span>

      <div class="swiper-wrapper">

        <div class="product swiper-slide">

          <div class="product-img">
            <img src="{$img_dir}compressed/illu-chaise.jpg" />
            <div class="promo"> <img src="{$img_dir}compressed/produit-info-20pc.png" /> </div>
          </div>

          <p class="product-title">
            CHAISE DESIGN EN POLYCARBONATE
          </p>
          <p class="product-price">
            <span class="price-old">149<sup>€00</sup></span>
            <span class="price-new">119<sup>€00</sup></span>
          </p>

        </div>
        <div class="product swiper-slide">

          <div class="product-img">
            <img src="{$img_dir}compressed/illu-chaise.jpg" />
            <div class="promo"> <img src="{$img_dir}compressed/produit-info-new.png" /> </div>
          </div>

          <p class="product-title">
            CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
          </p>
          <p class="product-price">
            <span class="price">119<sup>€00</sup></span>
          </p>

        </div>
        <div class="product swiper-slide">

          <div class="product-img">
            <img src="{$img_dir}compressed/illu-chaise.jpg" />
            <div class="promo"> <img src="{$img_dir}compressed/produit-info-10pc.png" /> </div>
          </div>

          <p class="product-title">
            CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
          </p>
          <p class="product-price">
            <span class="price">119<sup>€00</sup></span>
          </p>

        </div>

      </div>

      <div class="swiper-navigation-right">
        <div class="swiper-navigation-container">
          <span class="icon-right_arrow"></span>
        </div>
      </div>

    </div>

  </section>
{/if}
