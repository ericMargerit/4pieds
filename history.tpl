<nav id="navigation-account">
  <ul class="swiper-wrapper">
    <li class="swiper-slide"><a href="#" {if $smarty.get.page == 1}class="active"{/if}>Mes Envies</a></li>
    <li class="swiper-slide"><a class="active" href="#">Mes Commandes</a></li>
    <li class="swiper-slide"><a href="#" {if $smarty.get.page == 2}class="active"{/if}>Mon Profil</a></li>
    <li class="swiper-slide"><a href="#" {if $smarty.get.page == 3}class="active"{/if}>Mes Adresses</a></li>
  </ul>
</nav>

<section id="order-history">
  <ul >
    <li>
      <div class="order-date">
        <span class="icon icon-right_arrow"></span> <a href="#">01.02.2016</a> 
        <span class="order-price">160<sup>€00</sup></span>
        <ul class="order-details">
          <li>
            <span class="order-product-name">1 X CHAISE DESIGN EN PLEXITRANSPARENT ALLEGRA</span>
            <span class="order-product-price">80<sup>€00</sup></span>
          </li>
          <li> <a href="#" class="order-more-details">Détails de la commande</a> </li>
        </ul>
      </div>
      <div class="order-delivery-status">Livraison en cours</div>
      <div class="order-follow"><a href="#">Suivre la commande</a></div>
      <div class="clearfix"></div>
    </li>

    <li>
      <div class="order-date">
        <span class="icon icon-right_arrow"></span> <a href="#">01.02.2016</a> 
        <span class="order-price">160<sup>€00</sup></span>
        <ul class="order-details">
          <li>
            <span class="order-product-name">1 X CHAISE DESIGN EN PLEXITRANSPARENT ALLEGRA</span>
            <span class="order-product-price">80<sup>€00</sup></span>
          </li>
          <li> <a href="#" class="order-more-details">Détails de la commande</a> </li>
        </ul>
      </div>
      <div class="order-delivery-status delivery-ok">Livraison en cours</div>
      <div class="order-follow"><a href="#">Suivre la commande</a></div>
      <div class="clearfix"></div>
    </li>
  </ul>

  <div class="order-history-pagination">
    <span>commandes précédentes</span>
    <ul>
      <li class="active"><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
    </ul>
  </div>
</section>