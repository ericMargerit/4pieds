<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<html>
  <head>
    <meta charset="utf-8" />
    <title>4pieds</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="generator" content="PrestaShop" />
   	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
    <link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
    <link rel="stylesheet" href="{$css_dir}production.css" type="text/css" media="screen" />

  </head>
  <body {if !empty($smarty.get.popin) && $smarty.get.popin === "cookie"}class="no-scroll"{/if}>

    <section id="white-header">
      <div><span>Specialiste tables, chaises et tabourets de bar & snack,</span> n°1 en france depuis plus de 30 ans</div>
      <div> <a href="#"><strong>Écrivez-nous</strong></a> ou contactez-nous au <strong>02 99 05 37 00</strong> de 8h à 19h</div>
      <div class="clearfix"></div>
    </section>

    <header>


      <h1 id="logo"> <a href="/"><img src="{$img_dir}compressed/logo.png" alt="4 Pieds" /></a> </h1>
      <div id="mobile-header" class="only-small">
        <p>n°1 en france depuis plus de 30 ans</p>
        <div class="mobile-cart"> <span class="icon-cart"></span> <span class="cart-notifications">2</span> </div>
        <form method="POST" action=""> <input type="text" value="" /> <a href="#" class="search-submit"> <span class="icon-search"></span> </a>  </form>
      </div>

      <nav id="menu">
        <ul>
          <li class="only-small"> <a href="#" class="btn-menu"> <span class="icon-menu"></span> </a> </li>
          <li class="menu-item"> 
            <a href="#" data-menu="chaises"> <span class="icon-chaises"> &nbsp; </span> Chaises </a>
          </li>
          <li class="menu-item"> 
            <a href="#" data-menu="tabourets"> <span class="icon-tabouret"> &nbsp; </span> Tabourets </a> 
          </li>
          <li class="menu-item"> 
            <a href="#" data-menu="tables"> <span class="icon-table"> &nbsp; </span> Tables </a> 
          </li>
        </ul>
      </nav>

      <nav id="desktop-menu" class="no-small">
        <!-- Tablette & Desktop -->
        <li class="menu-item" > 
          <a href="#" data-menu="stores"> 
            <span class="sprite-stores"> &nbsp; </span> <span class="menu-item-label">Nos 24 <br class="no-large"/> magasins</span> 
            <span class="only-large">Rennes<br/> <span class="grey">0123456789</span> </span>
          </a>
        </li>
        <li class="menu-item" > 
          <a href="#" {if $smarty.get.connecte === "true"}data-menu="account"{else}data-menu="identification"{/if}> 
            <span class="sprite-account"> &nbsp; </span> <span class="menu-item-label">Mon <br class="no-large"/> compte </span> 
            <span class="only-large">Benedict Cumberbatch</span> 
          </a>
        </li>
        <li class="menu-item" > <a href="#" data-menu="cart"> <span class="sprite-cart-white"> &nbsp; </span> <span class="cart-notifications">2</span> <span class="cart-price">1350<sup>€00</sup></span> </a></li>
        <br class="clear"/>
      </nav>

      <section id="desktop-search" class="no-small">
        <form method="POST" action=""> 
          <input type="text" value="" placeholder="RECHERCHER, table scandinave, chaise polycarbonate..." /> 
          <a href="#" class="search-submit"> <span class="icon-search"></span> </a>  
        </form>
        <ul id="desktop-sidelinks">
          <li> 
            <a href="#" class="btn-submenu" data-menu="style"><span>Styles & Tendances</span> </a>
            <ul class="submenu">
              <li></li>
            </ul>
          </li>
          <li> <a href="#"><span>Professionnels</span> </a> </li>
          <li> <a href="#"><span>Notre blog design</span> </a> </li>
          <li> <a href="#"><span>C'est parti pour les soldes</span> </a> </li>
        </ul>
      </section>

      <div class="clearfix"></div>

    </header>

    {if $page_name == 'index'}

      <section id="slider-home" class="slider">

        {if $page_name == 'index'}
          <div id="event-bar" style="background:#0071BC url('{$img_dir}compressed//barre-solde.png') no-repeat center top;">
            <div id="event-bar-content" style="color:#fff;" >
              <strong style="background-color:#fff;color:#0071BC;">Derniers jours des SOLDES !</strong>
              Profitez des promos en nocturne : nos conseillers à votre écoute jusqu’à 22h ! <a href="#">Éventuel lien cliquable</a>
            </div>
          </div>
        {/if}

        <div class="swiper-wrapper">
          <div class="swiper-slide" style="background:transparent url({$img_dir}compressed/slider.jpg) no-repeat center top;"> 
            <div class="slide-content">
              <span class="slide-title">vos Produits personnalisés en 3 clics</span>
              <ul>
                <li>
                  <span class="slide-sprite sprite-slide-loupe"></span>
                  <span class="slide-label"> <strong>CHOISISSEZ</strong> <small>parmi plus de 5000 produits</small> </span>
                </li>
                
                <li>
                  <span class="slide-sprite sprite-slide-metre"></span>
                  <span class="slide-label"> <strong>Personnalisez</strong> <small>dimension, couleur, finition...</small> </span>
                </li>
                
                <li>
                  <span class="slide-sprite sprite-slide-cart"></span>
                  <span class="slide-label"> <strong>commandez</strong> <small>livraison dans nos 24 magasins ou à domicile</small> </span>
                </li>
              </ul>
            </div>
            <img src="" /> 
          </div>
          <div class="swiper-slide" style="background:transparent url({$img_dir}compressed/slider2.jpg) no-repeat center top;"><a href="#"></a> </div>
          <div class="swiper-slide" style="background:transparent url({$img_dir}compressed/slider3.jpg) no-repeat center top;"><a href="#"></a> </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
      </section>
    {/if}

    {if $page_name !== 'index'}
      {include file="$tpl_path./breadcrumb.tpl"}
    {/if}

    <section id="wrapper">
