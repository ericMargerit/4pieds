module.exports = function (grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    concat: {
      dist: {
        src: [
          'js/jquery-1.12.3.min.js',
          'js/swiper.jquery.min.js',
          'js/jquery.sticky.js',
          'js/jquery.mCustomScrollbar.concat.min.js',
          'js/jquery.customSelect.min.js',
          'js/jquery.hoverIntent.minified.js',
          'js/global.js',
          'js/product.js',
          'js/swiper.js'
        ],
        dest: 'js/production.js'
      }
    },
    uglify: {
      build: {
        src: 'js/production.js',
        dest: 'js/production.min.js'
      }
    },
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "css/style.css": ["less/style.less", "less/mixins.less"], // destination file and source file,
          //"css/hover.css": ["less/hover-css/hover.less"]
        }
      }
    },
    cssmin: {
      options: {
        shorthandCompacting: true,
        roundingPrecision: -1
      },
      target: {
        files: {
          'css/production.css': [
            'css/bootstrap.min.css',
            'css/swiper.min.css',
            'css/jquery.mCustomScrollbar.min.css',
            //'css/hover.css',
            'css/style.css'
          ]
        }
      }
    },
    imagemin: {
      png: {
        options: {
          optimizationLevel: 7
        },
        files: [
          {
            // Set to true to enable the following options…
            expand: true,
            // cwd is 'current working directory'
            cwd: 'img/',
            src: ['*.png','menu-pictos/*.png'],
            // Could also match cwd line above. i.e. project-directory/img/
            dest: 'img/compressed/',
            ext: '.png'
          }
        ]
      },
      jpg: {
        options: {
          progressive: true
        },
        files: [
          {
            // Set to true to enable the following options…
            expand: true,
            // cwd is 'current working directory'
            cwd: 'img/',
            src: ['*.jpg'],
            // Could also match cwd. i.e. project-directory/img/
            dest: 'img/compressed/',
            ext: '.jpg'
          }
        ]
      }
    },
    watch: {
      styles: {
        files: ['less/*.less', 'js/*.js'/*,'img/*.jpg','img/*.png'*/], // which files to watch
        tasks: ['less', 'concat', 'uglify', 'cssmin'/*,'imagemin'*/],
        options: {
          nospawn: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.registerTask('default', ['less', 'concat', 'uglify', 'cssmin', 'imagemin' ,'watch']);
};