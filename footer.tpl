
</section>

{if $page_name !== "order" && $page_name !== "my-account" && $page_name !== "history" }
<footer>
  
  <section id="footer-social">
    <div class="footer-social-left">
      <span class="span-title">SUIVEZ-NOUS !</span>
      <p>Rejoignez-nous sur nos réseaux sociaux pour suivre toute l'actualité de 4Pieds et ne rien rater des dernières nouveautés</p>
    </div>
    <ul class="footer-social-right">
      <li> <a href="#"> <span class="sprite-footer-social-facebook"></span> </a> </li>
      <li> <a href="#"> <span class="sprite-footer-social-twitter"></span> </a> </li>
      <li> <a href="#"> <span class="sprite-footer-social-pinterest"></span> </a> </li>
      <li> <a href="#"> <span class="sprite-footer-social-instagram"></span> </a> </li>
    </ul>
    <div class="clearfix"></div>
  </section>
  
  <section id="footer-professional-space">
    <span class="sprite-pro-chair"></span>
    <span class="span-title">ESPACE<br/> PROFESSIONNELS<br/> & COLLECTIVITÉS</span>
    <p>Des produits parfaitement adaptés à un usage professionnel (restaurant, salles de séminaires, hôtels...)</p>
    <a href="#">&nbsp;</a>
  </section>

  <section id="footer-cgv">
    <p>
      <span class="span-bold">CONDITIONS DES OFFRES -10% et -15%</span>
      Offres valables du 06/01/16 au 01/02/16 :
      * -10% sur toutes les chaises et tabourets sans minimum d’achat. 
      *-15% sur toutes nos tables pour un montant minimum de 500€ TTC. 
      Ces deux offres ne sont pas applicables sur les frais de transport, ni sur l’éco-participation mobilier. 
      Offres non cumulables avec les Soldes, codes promotionnels et devis en cours. Tout achat passé en dehors de ces dates ne pourra bénéficier des offres de remises. 
      Offres valables dans les magasins 4 PIEDS et sur le site internet www.4-pieds.com.
    </p>
  </section>

  <section id="footer-popular-search">
    <span class="span-title">recherches populaires</span>
    <ul>
      <li><a href="#">chaise de cuisine</a></li>
      <li><a href="#">chaise de salle à manger </a></li>
      <li><a href="#">chaise tripp trapp Stokke® </a></li>
      <li><a href="#">chaise en plexi </a></li>
      <li><a href="#">chaise en bois  </a></li>
      <li><a href="#">chaise en métal </a></li>
      <li><a href="#">chaise en fer forgé </a></li>
      <li><a href="#">chaise en cuir </a></li>
      <li><a href="#">chaise design </a></li>
      <li><a href="#">chaise contemporaine </a></li>
      <li><a href="#">chaise rustique </a></li>
      <li><a href="#">chaise de jardin</a></li>
      <li><a href="#">chaise ergonomique </a></li>
      <li><a href="#">chaise de bar</a></li>
    </ul>
  </section>

  <section id="footer-sitemap">

    <div id="sitemap-logo"> <img src="{$img_dir}compressed/logo.png" /> </div>
    <ul>
      <li><a href="#">Qui sommes-nous ?</a></li>
      <li><a href="#">Rejoignez-nous</a></li>
      <li><a href="#">Politique de Cookies</a></li>
      <li><a href="#">CGV</a></li>
      <li><a href="#">Mentions légales</a></li>
      <li><a href="#">Questions fréquentes</a></li>
      <li><a href="#">Espace des Marques</a></li>
      <li><a href="#">Lexique</a></li>
    </ul>

  </section>

  <div class="clearfix"></div>

</footer>
{/if}
<nav id="menu-float">
  <a class="close"> <span class="icon-close"></span> </a>
  <ul id="top-menu-float">
    <li> <a href="#"> <span class="sprite-stores"> &nbsp; </span> Nos 24 magasins </a></li>
    <li> <a href="#"> <span class="sprite-account"> &nbsp; </span> Mon compte </a></li>
    <li> <a href="#" id="top-menu-float-cart"> <span class="icon-cart"> &nbsp; </span> <span class="red">Panier (2)</span> <span class="cart-notifications">2</span> <span class="cart-price">1350<sup>€00</sup></span> </a></li>
  </ul>
  <ul id="core-menu-float">
    <li> 

      <div class="menu-red-block"> <span class="icon-mail"></span> écrivez-nous</div>
      <div class="menu-red-block"> <span class="icon-phone"></span> 02 99 05 37 00 <br/> <small>de 08h à 19h</small> </div>

    </li>
    <li> 
      <a href="#" class="btn-submenu"><span class="red">Chaises</span> <span class="icon-plus"></span></a>
      <ul class="submenu">
        <li> 
          <h5>Utilisation</h5>
          <ul>
            <li> <a href="#">Chaises de cuisine</a> </li>
            <li> <a href="#">Chaises de salles à manger</a> </li>
          </ul>
        </li>
        <li> 
          <h5>Matière</h5>
          <ul>
            <li> <a href="#">Chaises en plexi</a> </li>
          </ul>
        </li>
        <li class="submenu-grid"> 

          <ul>
            <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-1.png" alt=""/> Chaises pliantes</a> </li>
            <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-2.png" alt=""/>Chaises empliables</a> </li>
            <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-3.png" alt=""/>Chaises pivotantes</a> </li>
            <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-4.png" alt=""/>Fauteuils</a> </li>
            <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-5.png" alt=""/>Poufs blanc banquettes</a> </li>
            <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-6.png" alt=""/>Chaises ergonomiques</a> </li>
            <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-7.png" alt=""/>Nos ensembles</a> </li>
            <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-8.png" alt=""/>Accessoires assises</a> </li>
            <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-9.png" alt=""/>Produits d'entretien</a> </li>
            <li> <a href="#"><img src="{$img_dir}compressed/grid-link1.jpg" /></a> </li>
            <li> <a href="#"><img src="{$img_dir}compressed/grid-link2.jpg" /></a> </li>
            <li> <a href="#"><img src="{$img_dir}compressed/grid-link3.jpg" /></a> </li>
          </ul>

        </li>
      </ul>
    </li>
    <li> 
      <a href="#" class="btn-submenu"><span class="red">Tabourets</span> <span class="icon-plus"></span></a>
      <ul class="submenu">
        <li></li>
      </ul>
    </li>
    <li> 
      <a href="#" class="btn-submenu"><span class="red">Tables</span> <span class="icon-plus"></span></a>
      <ul class="submenu">
        <li></li>
      </ul>
    </li>
    <li> 
      <a href="#" class="btn-submenu"><span>Styles & Tendances</span> <span class="icon-plus"></span></a>
      <ul class="submenu">
        <li></li>
      </ul>
    </li>
    <li> <a href="#"><span>Professionnels</span> </a> </li>
    <li> <a href="#"><span>Notre blog design</span> </a> </li>
    <li> <a href="#"><span>C'est parti pour les soldes</span> </a> </li>
  </ul>
</nav>

<nav id="menu-float-desktop" class="no-small">

  <!-- Menu desktop Produits ( ex: chaises ) -->
  <div id="menu-float-desktop-chaises">
    <ul>
      <li>
        <h5>Utilisation</h5>
      </li>
      <li>
        <ul class="menu-float-desktop-list-links">
          <li> <a href="#">Chaises de cuisine</a> </li>
          <li> <a href="#">Chaises de salle à manger</a> </li>
          <li> <a href="#">Chaises de bar</a> </li>
          <li> <a href="#">Chaises de jardin</a> </li>
          <li> <a href="#">Chaises bébé et enfant</a> </li>
          <li> <a href="#">Chaises et fauteuils de bureau</a> </li>
        </ul>
      </li>
    </ul>

    <ul>
      <li>
        <h5>Matière</h5>
      </li>
      <li>
        <ul class="menu-float-desktop-list-links">
          <li> <a href="#">Chaises en plexi</a> </li>
          <li> <a href="#">Chaises en bois</a> </li>
          <li> <a href="#">Chaises en métal</a> </li>
          <li> <a href="#">Chaises en fer forgé</a> </li>
          <li> <a href="#">Chaises en plastique</a> </li>
          <li> <a href="#">Chaises en rotin et loom</a> </li>
          <li> <a href="#">Chaises en croûte de cuir</a> </li>
        </ul>
      </li>
    </ul>

    <ul>
      <li>
        <h5>style</h5>
      </li>
      <li>
        <ul class="menu-float-desktop-list-links">
          <li> <a href="#">Chaises design</a> </li>
          <li> <a href="#">Chaises modernes</a> </li>
          <li> <a href="#">Chaises contemporaines</a> </li>
          <li> <a href="#">Chaises traditionnelles</a> </li>
        </ul>
      </li>
    </ul>

    <ul class="submenu-grid">
      <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-1.png" alt=""/>Chaises pliantes</a> </li>
      <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-2.png" alt=""/>Chaises empliables</a> </li>
      <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-3.png" alt=""/>Chaises pivotantes</a> </li>
      <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-4.png" alt=""/>Fauteuils</a> </li>
      <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-5.png" alt=""/>Poufs blanc banquettes</a> </li>
      <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-6.png" alt=""/>Chaises ergonomiques</a> </li>
      <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-7.png" alt=""/>Nos ensembles</a> </li>
      <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-8.png" alt=""/>Accessoires assises</a> </li>
      <li> <a href="#"><img class="menu-picto" src="{$img_dir}compressed/menu-pictos/pictos-9.png" alt=""/>Produits d'entretien</a> </li>
    </ul>
    <ul class="submenu-grid submenu-img">
      <li> <a href="#"><img src="{$img_dir}compressed/grid-link1.jpg" /></a> </li>
      <li> <a href="#"><img src="{$img_dir}compressed/grid-link2.jpg" /></a> </li>
      <li> <a href="#"><img src="{$img_dir}compressed/grid-link3.jpg" /></a> </li>
    </ul>

    <div class="clearfix"></div>

  </div>
    
  <!-- Menu desktop : Menu de login -->
  <div id="menu-float-desktop-identification">
    <span class="span-title">Je m'identifie</span>
    <p class="error-message">
      Identifiant ou mot de passe incorrect
    </p>
    <form>
      <fieldset>
        <input type="text" placeholder="Adresse Email" class="error" />
        <span class="identification-checkbox"> <input type="checkbox" id="already_client"/> </span> <label for="already_client">Je suis déjà client</label>
        <div class="clearfix"></div>
        <p id="identification-password">
          <input type="text" placeholder="Mot de passe" />
          <a href="#">Mot de passe oublié</a>
          <input type="submit" value="Se Connecter"/>
        </p>
        <p id="identification-register">
          <a href="#">Créer mon compte</a>
        </p>
        ou
        <p>
          <a href="#" id="identification-facebook">Connexion facebook</a>
        </p>
      </fieldset>
    </form>
  </div>
    
  <!-- Menu desktop Mon Compte -->
  <div id="menu-float-desktop-account">
    <ul>
      <li><a href="#">Mes Envies <span class="icon-heart"> <span class="wish-number">2</span> </span> </a></li>
      <li><a href="#">Mes Commandes</a></li>
      <li><a href="#">Mon Profil</a></li>
      <li><a href="#">Mes Adresses</a></li>
      <li><a href="#">Se déconnecter</a></li>
    </ul>
  </div>

  <!-- Menu desktop Store Locator -->
  <div id="menu-float-desktop-stores" class="storelocator">

    <div class="storelocator-select">
      <span class="span-title"> 4pieds <br/> un réseau de 24 <br/> magasins partout en <br/> france </span>
      <select>
        <option>Choisissez un magasin</option>
        <option>Paris</option>
        <option>Rennes</option>
      </select>
    </div>

    <div class="storelocator-photo">
      <img src="{$img_dir}compressed/magasin.jpg" />
    </div>

    <div class="storelocator-infos">

      <span class="storelocator-city">Rennes</span>
      <p class=storelocator-adress>
        245 Route de Saint-Malo<br/>
        35000 Rennes
      </p>
      <p class="storelocator-hours">
        OUVERT<br/>
        <small>09:30 - 12:30</small> <br/>
        <small>14:00 - 19:00</small>
      </p>
      <div class="storelocator-opening">
        ouverture exceptionnelle
        lundi de pâques 10:00 - 18:00 
      </div>
      <span class="storelocator-phone">02 52 88 25 06</span>
      <a href="#">découvrez le magasin</a>
    </div>

    <div id="menu-desktop-storelocator-map">

    </div>

  </div>

  <!-- Menu desktop Styles et tendances -->
  <div id="menu-float-desktop-style">
    <ul>
      {for $foo=1 to 6}
        <li>
          <a href="#">
            <img src="{$img_dir}compressed/menu-tendance.jpg" />
            <div class="menu-style-link">
              <span>Scandinavie</span>
            </div>
          </a>
        </li>
      {/for}
    </ul>
  </div>

</nav>

<!-- Sticky Menu -->
<div id="sticky-menu">

  <div id="sticky-menu-left">
    {*<a href="#" id="sticky-menu-button" class="btn-menu"> <span class="icon-menu"></span> </a>*}
    <a href="#" id="sticky-menu-logo"><img src="{$img_dir}compressed/logo.png" alt="4 Pieds" /></a>
  </div>

  <div id="sticky-menu-right">
    <!-- Toute la partie est en float right, d'où l'inversement des blocs dans le HTML -->
    <div class="mobile-cart"> <span class="icon-cart"></span> <span class="cart-notifications">2</span> </div>
    {if $page_name === 'product'}
      <button class="product-add-wish"> <span class="icon-heart2"></span> <span class="no-small">Ajouter à mes envies</span> </button>
      <button class="product-add-cart"><span class="icon-cart"></span><span class="product-label-add-cart">Acheter</span></button>
      <span class="price-new no-small">199<sup>€99</sup></span>
    {/if}
    {if $page_name === "category"}
      <button class="category-sort">
        <span class="icon-zoom"></span>
        <span> 
          <strong>Affiner</strong>
        </span>
      </button>
    {/if}
    {if $page_name === "order"}

    {/if}

    {if $page_name === "product" || $page_name === "category"}
      <nav class="breadcrumb-4pieds">
        <ul>
          <li>Tables</li>
          <li>Tables Rectangulaires</li>
            {if $page_name === "product"}
            <li>
              CHAISE DESIGN EN POLYCARBONATE  OPAQUE STYLE RÉGENCE BELLE ÉPOQUE
              <small>ref : bakou</small>
            </li>
          {/if}
        </ul>
      </nav>
    {/if}
  </div>

</div>

{* sticky bar panier (bottom page) 
<div id="sticky-bar-cart">

  <div id="sticky-bar-cart-content">

    <div id="sticky-bar-cart-left">
      <span class="sprite-serenity4"></span>
      <div>
        une question sur le paiement ?
        besoin d’aide pour la livraison ?
      </div>
    </div>

    <div id="sticky-bar-cart-right">
      <button>
        <span class="sprite-bubble"></span>
        Chatter
      </button>
      <span id="sticky-bar-cart-callus">
        Appelez-nous <strong>02 99 05 37 00</strong>
      </span>
    </div>
  </div>
</div> *}

{if $page_name === "my-account"}
<div id="sticky-bar-cart">

  <ul id="sticky-bar-cart-content">
    <li>
      <a href="#">
        <span class="sprite-callback"></span>
        <span>Être rappelé</span>
        <span class="icon-up_arrow"></span>
      </a>
    </li>

    <li>
      <a href="#">
        <span class="sprite-chat"></span>
        <span>Chattez</span>
        <span class="icon-up_arrow"></span>
      </a>
    </li>

    <li>
      <a href="#">
        <span class="sprite-write-us"></span>
        <span>Ecrivez-nous</span>
        <span class="icon-up_arrow"></span>
      </a>
    </li>

    <li>
      <a href="#" class="open-bubble">
        <span class="sprite-contact-us"></span>
        <span>Nous contacter</span>
        <span class="icon-up_arrow"></span>
      </a>
      <div class="sticky-bar-bubble">
        <a href="#" class="close"><span class="icon-close"></span></a>
        Pour plus de renseignements,
        contactez-nous au :
        <strong class="red">02 99 05 37 00</strong>
        Du lundi au vendredi de
        8h à 18h sans interruption.
      </div>
    </li>

  </ul>

</div>
{/if}

<!-- sticky "besoin d'aide ?" -->
<div id="sticky-help" style="display:none;">

</div>

{if $smarty.get.popin == "cookie"}
  <!-- Popin de cookie avec geoloc -->
  <div id="cookie-popin">

    <a class="close" href="#">non merci <span class="icon-close"></span></a>

    <div id="cookie-popin-content">
      <div id="cookie-popin-left">
        <span id="cookie-popin-left-text1">Offre de<br/>bienvenue</span>
        <span id="cookie-popin-left-text2">Recevez</span>
        <span id="cookie-popin-left-text3">-15<sup>€</sup></span>
        <span id="cookie-popin-left-text4">à valoir sur votre<br/>première commande</span>
      </div>
      <form id="cookie-popin-right">
        <p>
          Découvrez en avant-première nos nouveautés en vous 
          inscrivant à notre newsletter
        </p>
        <fieldset>
          <select>
            <option> Choisir mon magasin... </option>
            <option> Rennes </option>
            <option> Paris </option>
          </select>
          <input type="text" value="" placeholder="Mon Email..." />
          <input type="submit" value="M'inscrire" />
        </fieldset>
      </form>
    </div>
    <div id="cookie-popin-bottom">
      Ce site utilise des cookies afin d’améliorer votre expérience. En continuant votre navigation, vous en acceptez l’usage.
      <a href="#">En savoir plus sur les cookies</a>
    </div>

  </div>
{/if}

{if $page_name === "product"}
  {*Barre de partage réseau sociaux sur la page produit*}
  <div id="product-share" class="only-large">
    <div id="product-share-left"><span>partager</span><span class="sprite-product-share"></span></div>
    <ul id="product-share-list">
      <li> <a href="#"> <span class="sprite-product-share-facebook"></span> </a> </li>
      <li> <a href="#"> <span class="sprite-product-share-twitter"></span> </a> </li>
      <li> <a href="#"> <span class="sprite-product-share-pinterest"></span> </a> </li>
      <li> <a href="#"> <span class="sprite-product-share-instagram"></span> </a> </li>
      <li> <a href="#"> <span class="sprite-product-share-google"></span> </a> </li>
      <li> <a href="#"> <span class="sprite-product-share-other"></span> </a> </li>
    </ul>
  </div>
{/if}

<div id="fullscreen-map-container">
  <a href="#" class="close"> <span class="icon-close"></span> </a>
  <div id="fullscreen-map"></div>
</div>

<div id="overlay"></div>
<div id="responsive-stylesheet">&nbsp;</div>

<script 
  src="https://maps.googleapis.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript" src="{$js_dir}production.min.js"></script>

</body>
</html>