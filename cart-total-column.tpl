<div id="order-infos">

  <div id="order-price">
    {if $smarty.get.page < 5}
      <span class="span-title">Montant total</span>
      <div id="order-subtotal"> 
        <span class="order-label">Produits(4):</span> <span class="order-price">1373<sup>€00</sup></span>
      </div>
      <div id="order-delivery"> 
        <span class="order-label">livraison estimée :</span>
        <div class="order-infos-link infobulle-container">
          ?
          <div id="order-infosbulle" class="infobulle-content">
            Les frais de livraison sont estimés en fonction du contenu de votre panier et en France métropolitaine. 
            Vous pourrez choisir un autre mode de livraison dès la prochaine étape si vous le souhaitez.
          </div>
        </div>
        <span class="order-label">154<sup>€95</sup></span> 
      </div>
      <div id="order-delivery-infos"> Ou retrait <span class="red">gratuit</span> en magasin </div>
    {elseif $smarty.get.page == 5}
      <span class="span-title">Récapitulatif</span>
      <div id="order-subtotal"> 
        <span class="order-label">Produits(4) :</span>
      </div>
      <ul id="order-summary">
        <li>
          <span class="order-label-product"> 1 x Chaise design en polycarbonate opaque style Régence Belle Epoque</span>
          <span class="order-label"> 119<sup>€00</sup> </span>
          <div class="clearfix"></div>
        </li>
        <li>
          <span class="order-label-product"> 1 x Chaise design en polycarbonate opaque style Régence Belle Epoque</span>
          <span class="order-label"> 119<sup>€00</sup> </span>
          <div class="clearfix"></div>
        </li>
        <li>
          <span class="order-label-product"> 1 x Chaise design en polycarbonate opaque style Régence Belle Epoque</span>
          <span class="order-label"> 119<sup>€00</sup> </span>
          <div class="clearfix"></div>
        </li>
      </ul>
      <hr/>
      <div id="order-delivery-summary">
        <span class="order-label">Livraison :</span>
        <div class="clearfix"></div>
        <span class="order-label-product">Livraison Expert sur rendez-vous</span>
        <span class="order-label"> 119<sup>€00</sup> </span>
      </div>
      <hr/>
    {/if}
    {if $smarty.get.page >= 2}
      <div id="order-vouchers">
        <span class="order-label">code avantage :</span>
        <span class="order-voucher"> code fidélité -10% <span class="icon-close"></span> </span>
        <span class="order-label">154<sup>€95</sup></span> 
      </div>
    {/if}
    <hr/>
    <div id="order-total"> 
      <span class="order-price">1373<sup>€00</sup></span> 
      <div class="clearfix"></div>
    </div>
    {if $smarty.get.page == 1}
      <form>
        <fieldset class="error">
          <input type="text" placeholder="Code avantage" />
          <input type="submit" value="+" />
        </fieldset>
        <p class="error-message-light">Code avantage non valide</p>
      </form>
      <a id="order-validation-cart" href="#"><span class="icon-validation"></span>Valider mon panier</a>
    {elseif $smarty.get.page == 2}
      <a id="order-validation-address" href="#"><span class="icon-validation"></span>Valider mon adresse</a>
    {elseif $smarty.get.page == 3}
      <a id="order-validation-delivery" href="#"><span class="icon-validation"></span>Valider ma livraison</a>
    {elseif $smarty.get.page == 4}
      <a id="order-validation-cart" href="#"><span class="icon-validation"></span>Valider mon adresse</a>
    {elseif $smarty.get.page == 5}

      <div id="order-delivery-address">
        <span class="order-label">Adresse de Livraison:</span>
        <div class="clearfix"></div>
        <p class="order-label-product">
          Benedict Cumberbatch<br/>
          13 place des Lices<br/>
          35000<br/>
          Rennes<br/>
          FRANCE
        </p>
      </div>

      <div id="order-invoice-address">
        <span class="order-label">Adresse de Facturation:</span>
        <div class="clearfix"></div>
        <p class="order-label-product">
          Benedict Cumberbatch<br/>
          13 place des Lices<br/>
          35000<br/>
          Rennes<br/>
          FRANCE
        </p>
      </div>

    {/if}
  </div>

  <div id="order-wish-list" class="only-small">
    <ul>
      <li class="product"> 

        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
        </div>

        <p class="product-title">
          CHAISE DESIGN EN POLYCARBONATE 
        </p>
        <p class="product-price">
          <span class="price">119<sup>€00</sup></span>
        </p>

        <div class="wishlist-details">

          <p>
            <span class="wish-product-color"> 
              <span class="product-select-color-style product-select-color1"/></span> Noir
            </span>
            Structure : métal chromé<br/>
            Hauteur avec dossier : 80 cm<br/>
            Profondeur : 50 cm<br/>
            Largeur : 60 cm
          </p>

          <form>
            <fieldset>
              <select>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
              </select>
            </fieldset>

            <button class="product-add-cart"><span class="icon-cart"></span><span class="product-label-add-cart">Commander</span></button>

          </form>

        </div>

      </li>
    </ul>
  </div>

  <div id="order-serenity">
    <span class="span-title">sérénité garantie</span>
    <ul>
      <li>
        <span class="sprite-serenity1"></span>
        <p>
          <strong>3x sans frais<br/> à partir de 300€</strong>
          CB - PayPal - Chèque - Virement
        </p>
      </li>
      <li>
        <span class="sprite-serenity2"></span>
        <p><strong>produits garantis 2 ans</strong></p>
      </li>
      <li>
        <span class="sprite-serenity3"></span>
        <p>
          <strong>Satisfait ou remboursé</strong>
          14 jours pour décider
        </p>
      </li>
      <li>
        <span class="sprite-serenity4"></span>
        <p>
          <strong>service client <span class="red">02 99 05 37 00</span></strong>
          Du lundi au vendredi de 8h à 19h
          Appel non surtaxé
        </p>
      </li>
    </ul>
  </div>

</div>