<section id="product-container">

  <div id="product-big-img">
    <img src="{$img_dir}compressed/illu-chaise-big.jpg" />
    <span id="product-code">code : 049</span>
  </div>

<!-- ma teinte -->
{if $product->id == 5}

  <div id="product-infos">

    <div class="product-price">
      <span class="price-old">149<sup>€00</sup></span>
      <span class="price-new">119<sup>€00</sup></span>
      <span class="price-details">HT : 99€37</span>
      <span class="price-details">dont écotaxe 0€21</span>
    </div>

    <div class="product-boxes">
      <span> <img src="{$img_dir}compressed/produit-info-20pc.png"/> </span>
      <span> <img src="{$img_dir}compressed/produit-info-new.png"/> </span>
    </div>

    <div class="product-price-extra">
      <span class="price-credit">ou 3 x <strong>492<sup>€09</sup></strong> sans frais</span>
    </div>

    <div class="product-review">
      <span class="product-stars"> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star"></span> </span>
      <span class="product-comments"> 4 commentaires </span>
    </div>

    <form class="product-quantity">
      <select class="quantity-selector" name="product-quantity">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option class="more">+</option>
      </select>
    </form>

    <div class="product-delivery">
      <span>
        livraison en magasin gratuite - RENNES
        <br/>
        à domicile, à partir de 13€95
      </span>
      <span class="product-tag en-stock"> en stock <span>livrés chez vous sous 48h / 72h</span></span> <br/>
      <span class="product-tag on-demand"> à la demande <span>fabrication 6 semaines</span></span>
      <div class="info-on-demand-container infobulle-container">?
        <div class="info-on-demand infobull-content">
          <span class="span-title">Produit fabriqué à la demande !</span>
          <p>Ce produit est fabriqué à votre demande selon des critères de personnalisation mis à votre disposition, c'est pourquoi il ne sera ni repris ni échangé.</p>
        </div>
      </div>
    </div>


    <div class="product-personalize">
      <span class="product-personalize-title">Teinte de la structure(12+)</span>

      <form class="product-personalize-zone">
        <span class="product-personalize-custom-select">
          <a href="#"> <span class="product-select-color-style product-select-color1"></span> Polycarbonate </a>
        </span>
        <select class="product-personalize-select">
          <option value="1"> Polycarbonate</option>
          <option value="2"> Couleur 2</option>
          <option value="3"> Couleur 3</option>
          <option value="4"> Couleur 4</option>
        </select>

        <div class="product-personalize-selection teinte">
          <a href="#" class="close"> <span class="icon-close"></span> </a>
          <span class="span-title">Teinte de la structure(12+)</span>
          <div class="product-personalize-selected">
            <img data-color="1" src="{$img_dir}products-colors/back-teinte.png" alt="" />
            <a href="#" class="product-personalize-select-button">choisir</a>
            <div class="product-personalize-selected-txt">
              <p class="first-txt red">
                Une personnalisation infinie grâce à mes teintes personnalisées !
              </p>
              <p class="second-txt">
                Pour définir votre couleur spécifique, <br> contactez le <span class="number red">02 99 05 37 00</span>
              </p>
            </div>
            <span class="title">ma teinte</span>
          </div>
          <div class="product-personalize-list-container">
            <ul class="product-personalize-list">
              {for $foo=16 to 21}
                <li><a href="#"> <span data-color="{$foo}" class="product-select-color-style {if $foo == 1}product-select-color-selected{/if} product-select-color{$foo}"/>&nbsp;</span> </a></li>
              {/for}
            </ul>
            <div class="clearfix"></div>
            <div class="product-personalize-txt">
              <p>
                Le réglage de votre écran peut légèrement altérer les coloris
              </p>
            </div>
          </div>
          <button class="product-personalize-teinte-button">
            <img src="{$img_dir}/teinte-btn.png" alt="" />
            <span>demander échantillon</span>
          </button>
        </div>

      </form>

      <span class="product-personalize-title">Dimension(3)</span>
      <form>
        <select class="customSelect">
          <option value="1"> 140 x 100 cm + 2 allonges </option>
          <option value="2"> 240 x 100 cm + 4 allonges </option>
          <option value="3"> 540 x 100 cm + 6 allonges </option>
          <option value="4"> 840 x 100 cm + 8 allonges </option>
        </select>
      </form>
    </div>


    <div class="product-add">
      <button class="product-add-cart"><span class="icon-cart"></span><span class="product-label-add-cart">Ajouter au panier</span></button>
      <button class="product-add-wish"> <span class="icon-heart2"></span><span class="product-label-add-wish"> Ajouter à mes envies</span></button>
    </div>

  </div>

{else}

  <!-- normal -->

  <div id="product-infos">

    <div class="product-price">
      <span class="price-old">149<sup>€00</sup></span>
      <span class="price-new">119<sup>€00</sup></span>
      <span class="price-details">HT : 99€37</span>
      <span class="price-details">dont écotaxe 0€21</span>
    </div>

    <div class="product-boxes">
      <span> <img src="{$img_dir}compressed/produit-info-20pc.png"/> </span>
      <span> <img src="{$img_dir}compressed/produit-info-new.png"/> </span>
    </div>

    <div class="product-price-extra">
      <span class="price-credit">ou 3 x <strong>492<sup>€09</sup></strong> sans frais</span>
    </div>

    <div class="product-review">
      <span class="product-stars"> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star"></span> </span>
      <span class="product-comments"> 4 commentaires </span>
    </div>

    <form class="product-quantity">
      <select class="quantity-selector" name="product-quantity">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option class="more">+</option>
      </select>
    </form>

    <div class="product-delivery">
      <span>
        livraison en magasin gratuite - RENNES
        <br/>
        à domicile, à partir de 13€95
      </span>
      <span class="product-tag en-stock"> en stock <span>livrés chez vous sous 48h / 72h</span></span> <br/>
      <span class="product-tag on-demand"> à la demande <span>fabrication 6 semaines</span></span>
      <div class="info-on-demand-container infobulle-container">?
        <div class="info-on-demand infobull-content">
          <span class="span-title">Produit fabriqué à la demande !</span>
          <p>Ce produit est fabriqué à votre demande selon des critères de personnalisation mis à votre disposition, c'est pourquoi il ne sera ni repris ni échangé.</p>
        </div>
      </div>
    </div>


    <div class="product-personalize">
      <span class="product-personalize-title">Coloris(3)</span>

      <form class="product-personalize-zone">
        <span class="product-personalize-custom-select">
          <a href="#"> <span class="product-select-color-style product-select-color1"></span> Polycarbonate </a>
        </span>
        <select class="product-personalize-select">
          <option value="1"> Polycarbonate</option>
          <option value="2"> Couleur 2</option>
          <option value="3"> Couleur 3</option>
          <option value="4"> Couleur 4</option>
        </select>

        <div class="product-personalize-selection">
          <a href="#" class="close"> <span class="icon-close"></span> </a>
          <span class="span-title">Coloris(3)</span>
          <div class="product-personalize-selected">
            <img data-color="1" src="{$img_dir}products-colors/1.png" alt="" />
            <a href="#" class="product-personalize-select-button">choisir</a>
            <span class="title">polycarbonate jaune</span>
          </div>
          <div class="product-personalize-list-container">
            <ul class="product-personalize-list">
              {for $foo=1 to 12}
                <li><a href="#"> <span data-color="{$foo}" class="product-select-color-style {if $foo == 1}product-select-color-selected{/if} product-select-color{$foo}"/>&nbsp;</span> </a></li>
              {/for}
            </ul>
            <div class="clearfix"></div>
            <div class="product-personalize-txt">
              <p>
                Le réglage de votre écran peut légèrement altérer les coloris
              </p>
            </div>
          </div>
        </div>

      </form>

      <span class="product-personalize-title">Coloris bis(3)</span>

      <form class="product-personalize-zone">
        <span class="product-personalize-custom-select">
          <a href="#"> <span class="product-select-color-style product-select-color1"></span> Polycarbonate </a>
        </span>
        <select class="product-personalize-select">
          <option value="1"> Polycarbonate</option>
          <option value="2"> Couleur 2</option>
          <option value="3"> Couleur 3</option>
          <option value="4"> Couleur 4</option>
        </select>

        <div class="product-personalize-selection">
          <a href="#" class="close"> <span class="icon-close"></span> </a>
          <span class="span-title">Coloris bis(3)</span>
          <div class="product-personalize-selected">
            <img data-color="1" src="{$img_dir}products-colors/1.png" alt="" />
            <a href="#" class="product-personalize-select-button">choisir</a>
            <span class="title">polycarbonate jaune</span>
          </div>
          <div class="product-personalize-list-container">
            <ul class="product-personalize-list">
              {for $foo=1 to 4}
                <li><a href="#"> <span data-color="{$foo}" class="product-select-color-style {if $foo == 1}product-select-color-selected{/if} product-select-color{$foo}"/>&nbsp;</span> </a></li>
                {/for}
            </ul>
            <div class="clearfix"></div>
            <div class="product-personalize-txt">
              <p>
                Le réglage de votre écran peut légèrement altérer les coloris
              </p>
            </div>
          </div>
        </div>
      </form>

      <span class="product-personalize-title">Dimension(3)</span>
      <form>
        <select class="customSelect">
          <option value="1"> 140 x 100 cm + 2 allonges </option>
          <option value="2"> 240 x 100 cm + 4 allonges </option>
          <option value="3"> 540 x 100 cm + 6 allonges </option>
          <option value="4"> 840 x 100 cm + 8 allonges </option>
        </select>
      </form>
    </div>


    <div class="product-add">
      <button class="product-add-cart"><span class="icon-cart"></span><span class="product-label-add-cart">Ajouter au panier</span></button>
      <button class="product-add-wish"> <span class="icon-heart2"></span><span class="product-label-add-wish"> Ajouter à mes envies</span></button>
    </div>

  </div>

{/if}

  <div id="product-gallery">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <img src="{$img_dir}compressed/test-galery_01.jpg" />
      </div>
      <div class="swiper-slide small-imgs">
        <img src="{$img_dir}compressed/test-galery_02.jpg" />
        <img src="{$img_dir}compressed/test-galery_01.jpg" />
        <img src="{$img_dir}compressed/test-galery_02.jpg" />
        <img src="{$img_dir}compressed/test-galery_01.jpg" />
      </div>
      <div class="swiper-slide small-imgs">
        <img src="{$img_dir}compressed/test-galery_02.jpg" />
        <img src="{$img_dir}compressed/test-galery_01.jpg" />
        <img src="{$img_dir}compressed/test-galery_02.jpg" />
        <img src="{$img_dir}compressed/test-galery_01.jpg" />
      </div>
      <div class="swiper-slide">
        <img src="{$img_dir}compressed/test-galery_03.jpg" />
      </div>
    </div>
    <div class="swiper-navigation-right">
      <div class="swiper-navigation-container">
        <span class="icon-right_arrow"></span>
      </div>
    </div>
  </div>

  <div class="product-description">
    <p>
      La chaise Belle époque est une interprétation moderne de la chaise Louis 15 régence. La conception de cette chaise de salle à manger en polycarbonate opaque offre beaucoup d'avantages et en particulier le confort et la facilité d'entretien. Si vous désirez  moderniser votre salle à manger Régence, cette chaise sera du plus bel effet dans votre intérieur.
    </p>
    <p>
      Structure en polycarbonate azur, jaune, rouge, vert opaque.
    </p>
    <p>
      Moulage qui reproduit les détails des chaises régences avec les moulures, les fleurs sculptées, les pieds volutes, le cloutage à l'assise et au dossier.
    </p>
    <p>
      Ce produit n'est pas empilable.
    </p>
    <p>
      <span class="span-title">Conseil d’entretien</span>
      Nettoyer à sec avec un chiffon doux. Ne pas utiliser de produits à base d'alcool ou d'ammoniaque ou autres produits abrasifs.
    </p>
  </div>

  <div class="clearfix"></div>

  <div class="product-size">

    <span class="sprite-chair-size"></span>

    <div class="product-size-description">
      <span class="span-title"> Caractéristiques techniques </span>
      <p>
        a : 97 cm<br/>
        b : 55 cm<br/>
        c : 52.5 cm<br/>
        d : 47cm<br/>
        <br/>
        Hauteur d'assise (a) : 47 cm<br/>
        Hauteur avec dossier : 97 cm<br/>
        Largeur (c) : 52.5 cm<br/>
        Profondeur (b) : 55 cm<br/>
        <br/>
        Poids : 5 Kg<br/>
        <br/>
        <span class="span-bold">Livrée montée</span><br/>
        <span class="span-bold">Garantie : 2 ans</span><br/>
        <br/>
        Fabriqué en Italie
      </p>
    </div>
    <div class="clearfix"></div>
  </div>

  <div class="product-delivery-infos">

    <span class="span-title">Livraison</span>
    <span class="span-title">Retrait GRATUIT en magasin</span>
    <p>
      Evitez les frais de port en venant retirer votre commande dans l’un de nos 24 points de vente. Disponible sous 15 jours après validation de votre commande pour les produits en stock. Le magasin vous prévient dès que la commande peut être retirée.
    </p>

    <span class="span-title">Livraison sur rendez-vous</span>
    <p>
      Le transporteur vous appelle pour fixer un jour de livraison au pieds de l’immeuble ou en pas de porte du lundi au vendredi.
    </p>

    <span class="span-title">Livraison sur rendez-vous avec option livraison à l’étage</span>
    <p>
      Le transporteur vous appelle pour fixer un jour de livraison du lundi au vendredi.
    </p>

    <span class="span-title">Livraison sur rendez-vous avec option montage dans la pièce de votre choix</span>
    <p>
      Le transporteur vous appelle pour fixer un jour de livraison du lundi au vendredi.
    </p>

  </div>

  <div class="product-also-seen">

    <span class="span-title">Nos Clients ont également regardé</span>

    <div class="swiper-wrapper">

      <div class="product swiper-slide">

        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
          <div class="promo"> <img src="{$img_dir}compressed/produit-info-20pc.png" /> </div>
        </div>

        <p class="product-title">
          CHAISE DESIGN EN POLYCARBONATE
        </p>
        <p class="product-price">
          <span class="price-old">149<sup>€00</sup></span>
          <span class="price-new">119<sup>€00</sup></span>
        </p>

      </div>
      <div class="product swiper-slide">

        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
          <div class="promo"> <img src="{$img_dir}compressed/produit-info-new.png" /> </div>
        </div>

        <p class="product-title">
          CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
        </p>
        <p class="product-price">
          <span class="price">119<sup>€00</sup></span>
        </p>

      </div>
      <div class="product swiper-slide">

        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
          <div class="promo"> <img src="{$img_dir}compressed/produit-info-10pc.png" /> </div>
        </div>

        <p class="product-title">
          CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
        </p>
        <p class="product-price">
          <span class="price">119<sup>€00</sup></span>
        </p>

      </div>

    </div>

    <div class="swiper-navigation-right">
      <div class="swiper-navigation-container">
        <span class="icon-right_arrow"></span>
      </div>
    </div>

  </div>

  <div class="product-list-comments">

    <div class="product-list-comments-title">
      <span class="span-title">
        Commentaires des clients
        <span class="product-stars">
          <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star"></span>
        </span>
      </span>
      <span>Note moyenne : 4.3</span>
    </div>

    <div class="product-comment">
      <div class="product-comment-star"><span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star"></span></div>
      <span class="product-comment-date">12/05/2016</span><span class="product-comment-nickname">Simone !</span>
      <p>
        Produit bien dessiné et réalisation soignée
      </p>
    </div>

    <div class="product-comment">
      <div class="product-comment-star"><span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star star-yellow"></span> <span class="icon-star"></span></div>
      <span class="product-comment-date">12/05/2016</span><span class="product-comment-nickname">Simone !</span>
      <input type="checkbox" id="toggler">
      <div class="more">
        <p>
          Le style m'a accroché, parfait pour un intérieur moderne.
          Chaises de bonne qualité, assise très confortable.
          Juste un peu bruyantes sur le parquet mais détail facilement résolu par la pause de patins en feutre.
          Le seul point (très) négatif étant la
        </p>
        <p>Nunc congue aliquam metus, eget faucibus lacus tincidunt a. Nunc magna dolor, blandit non ultrices ac, suscipit ut erat. Donec congue porta neque, sed mattis felis varius sit amet. Curabitur lobortis eros ac erat vehicula, ut elementum diam sollicitudin. Phasellus consectetur tellus eget neque finibus, eget cursus mi consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Praesent feugiat condimentum justo, non placerat dolor sodales vitae. Morbi in egestas risus. Suspendisse pellentesque sit amet metus ut luctus. Quisque ac tortor quis dui vehicula pretium ut nec dolor. Etiam aliquet metus quam, et placerat elit viverra vitae. Ut interdum condimentum arcu pretium vehicula.</p>
        <label for="toggler" class="toggler-css"></label>
      </div>
    </div>

    <div class="product-list-comments-pagination">
      <span>commentaires suivants</span>
      <ul>
        <li class="no-small active"><a href="#">1</a></li>
        <li class="no-small"><a href="#">2</a></li>
        <li class="no-small"><a href="#">3</a></li>
        <li class="only-small"><a href="#">Suivante</a></li>
      </ul>
    </div>

    <div class="clearfix"></div>

  </div>



  {include file="$tpl_dir./bloc-promo-newsletter.tpl"}

  <div id="product-last-seen">

    <span class="span-title">Derniers articles vus</span>

    <ul id="product-last-seen-list">
      <li>
        <img src="{$img_dir}compressed/illu-chaise-big.jpg" />
      </li>
      <li>
        <img src="{$img_dir}compressed/illu-chaise-big.jpg" />
      </li>
      <li>
        <img src="{$img_dir}compressed/illu-chaise-big.jpg" />
      </li>
    </ul>
    <div class="clearfix"></div>
  </div>

  {include file="$tpl_dir./bloc-engagement.tpl"}

</section>

<!-- Popin d'Ajout au panier -->
<div id="add-cart-popin">

  <a href="#" class="close"> <span class="icon-close"></span> </a>

  <span class="span-title">Ce produit a bien été <span class="add-cart-title">ajouté à votre panier</span></span>

  <div id="add-cart-popin-top">
    <div class="add-cart-product-img">
      <img src="{$img_dir}compressed/illu-chaise-big.jpg" />
    </div>

    <div class="add-cart-product-infos-container">
      <div class="add-cart-product-infos">
        <span class="add-cart-product-name">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE ÉPOQUE</span>

        <div class="add-cart-product-personalize">
          <span class="product-select-color-style product-select-color1"></span> Polycarbonate rouge
        </div>
        <p class="add-cart-product-details">
          Hauteur avec dossier : 97 cm<br/>
          Largeur : 52.5 cm<br/>
          Poids : 5Kg
        </p>
        <div class="add-cart-product-tags">
          <span class="product-tag en-stock">en stock en ligne</span>
        </div>
      </div>
    </div>

    <div id="add-cart-popin-actions-top">
      <a href="#"><span class="icon-left-arrow"></span> poursuivre mes achats</a>
      <a href="#"><span class="icon-validation"></span> valider ma commande</a>
      <div class="clearfix"></div>
    </div>

  </div>

  <div id="add-cart-popin-bottom">

    <span class="span-title only-large">Produits souvent achetés ensemble</span>

    <div class="swiper-wrapper">

      <div class="product swiper-slide">
        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
        </div>
        <p class="product-title">
          CHAISE DESIGN EN POLYCARBONATE
        </p>
        <p class="product-price">
          <span class="price-old">149<sup>€00</sup></span>
          <span class="price-new">119<sup>€00</sup></span>
        </p>
        <div class="product-alt-colors">
          <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
        </div>
      </div>

      <div class="product swiper-slide">
        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
        </div>
        <p class="product-title">
          CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
        </p>
        <p class="product-price">
          <span class="price">119<sup>€00</sup></span>
        </p>
        <div class="product-alt-colors">
          <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
        </div>
      </div>

      <div class="product swiper-slide">
        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
        </div>
        <p class="product-title">
          CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
        </p>
        <p class="product-price">
          <span class="price">119<sup>€00</sup></span>
        </p>
        <div class="product-alt-colors">
          <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
        </div>
      </div>

      <div class="product swiper-slide">
        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
        </div>
        <p class="product-title">
          CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
        </p>
        <p class="product-price">
          <span class="price">119<sup>€00</sup></span>
        </p>
        <div class="product-alt-colors">
          <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
        </div>
      </div>

    </div>

    <div class="swiper-navigation-right">
      <div class="swiper-navigation-container">
        <span class="icon-right_arrow"></span>
      </div>
    </div>

    <div id="add-cart-popin-actions-bottom">
      <a href="#"><span class="icon-left-arrow"></span> poursuivre mes achats</a>
      <a href="#"><span class="icon-validation"></span> valider ma commande</a>
    </div>

  </div>

</div>
