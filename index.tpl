
{* BLOC TOP PRODUITS *}

<section id="top-products">

  <h2>Nos Produits à la Une</h2>

  <div class="swiper-wrapper">

    <div class="product product-hover swiper-slide"> 

      <div class="product-img">
        <img src="{$img_dir}compressed/illu-chaise.jpg" />
        <div class="promo"> <img src="{$img_dir}compressed/produit-info-20pc.png" /> </div>
      </div>

      <p class="product-title">
        <a class="extend-click-a" href="http://www.4-pieds.com/">CHAISE DESIGN EN POLYCARBONATE</a>
      </p>
      <p class="product-price">
        <span class="price">119<sup>€00</sup></span>
      </p>

      <div class="product-alt-colors">
        <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
      </div>

      <ul class="product-color-selector">
        <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
        <li>
          <ul class="product-color-selector-submenu">
            <li class="product-color-selector-content"> <a href="#" title="color 1"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#" title="color 2"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#" title="color 3"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#" title="color 4"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#" title="color 5"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#" title="color 6"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#" title="color 7"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#" title="color 8"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#" title="color 9"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#" title="Plus de coloris"> <span class="product-select-color-style product-select-more"/>+</span> <span class="product-select-color-label no-large">Plus de coloris</span> </a> </li>
          </ul>
        </li>
      </ul>

    </div>
    <div class="product product-hover swiper-slide"> 

      <div class="product-img">
        <img src="{$img_dir}compressed/illu-chaise.jpg" />
        <div class="promo"> <img src="{$img_dir}compressed/produit-info-new.png" /> </div>
      </div>

      <p class="product-title">
        <a class="extend-click-a" href="#">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE</a>
      </p>
      <p class="product-price">
        <span class="price-old">149<sup>€00</sup></span>
        <span class="price-new">119<sup>€00</sup></span>
      </p>

      <div class="product-alt-colors">
        <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
      </div>

      <ul class="product-color-selector">
        <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
        <li>
          <ul class="product-color-selector-submenu">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
          </ul>
        </li>
      </ul>

    </div>
    <div class="product product-hover swiper-slide"> 

      <div class="product-img">
        <img src="{$img_dir}compressed/illu-chaise.jpg" />
        <div class="promo"> <img src="{$img_dir}compressed/produit-info-10pc.png" /> </div>
      </div>

      <p class="product-title">
        <a class="extend-click-a" href="#">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE</a>
      </p>
      <p class="product-price">
        <span class="price">119<sup>€00</sup></span>
      </p>

      <div class="product-alt-colors">
        <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
      </div>

      <ul class="product-color-selector">
        <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
        <li>
          <ul class="product-color-selector-submenu">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
          </ul>
        </li>
      </ul>

    </div>
    <div class="product product-hover swiper-slide"> 

      <div class="product-img">
        <img src="{$img_dir}compressed/illu-chaise.jpg" />
        <div class="promo"> <img src="{$img_dir}compressed/produit-info-10pc.png" /> </div>
      </div>

      <p class="product-title">
        <a class="extend-click-a" href="#">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE</a>
      </p>
      <p class="product-price">
        <span class="price">119<sup>€00</sup></span>
      </p>

      <div class="product-alt-colors">
        <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
      </div>

      <ul class="product-color-selector">
        <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
        <li>
          <ul class="product-color-selector-submenu">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
          </ul>
        </li>
      </ul>

    </div>
    <div class="product product-hover swiper-slide"> 

      <div class="product-img">
        <img src="{$img_dir}compressed/illu-chaise.jpg" />
        <div class="promo"> <img src="{$img_dir}compressed/produit-info-10pc.png" /> </div>
      </div>

      <p class="product-title">
        <a class="extend-click-a" href="#">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE</a>
      </p>
      <p class="product-price">
        <span class="price">119<sup>€00</sup></span>
      </p>

      <div class="product-alt-colors">
        <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
      </div>

      <ul class="product-color-selector">
        <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
        <li>
          <ul class="product-color-selector-submenu">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
          </ul>
        </li>
      </ul>

    </div>
    <div class="product product-hover swiper-slide"> 

      <div class="product-img">
        <img src="{$img_dir}compressed/illu-chaise.jpg" />
        <div class="promo"> <img src="{$img_dir}compressed/produit-info-10pc.png" /> </div>
      </div>

      <p class="product-title">
        <a class="extend-click-a" href="#">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE</a>
      </p>
      <p class="product-price">
        <span class="price">119<sup>€00</sup></span>
      </p>

      <div class="product-alt-colors">
        <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
      </div>

      <ul class="product-color-selector">
        <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
        <li>
          <ul class="product-color-selector-submenu">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
          </ul>
        </li>
      </ul>

    </div>

  </div>

  <div class="swiper-navigation-right">
    <div class="swiper-navigation-container">
      <span class="icon-right_arrow"></span>
    </div>
  </div>

</section>

{* BLOC MADE IN FRANCE *}

<section id="made-in-france">

  <div id="made-in-france-wrapper">
    <div id="made-in-france-content">
      <img src="{$img_dir}compressed/france.png" width="80" height="83"/>
      <span class="span-title">fabrication française</span>
      <p>
        Retrouvez la rigueur et la qualité de la fabrication française en favorisant l’emploi et en réduisant les émissions de CO2.
      </p>
    </div>
  </div>
  <ul id="made-in-france-sidelinks">
    <li> 
      <a href="#">
        <span class="icon-chaises"></span>
        <span><strong>Chaises</strong> Fabriquées en france</span>
        <span class="icon-right_arrow"></span>
      </a> 
    </li>
    <li> 
      <a href="#">
        <span class="icon-tabouret"></span>
        <span><strong>Tabourets</strong> Fabriqués en france</span>
        <span class="icon-right_arrow"></span>
      </a> 

    </li>
    <li> 
      <a href="#">
        <span class="icon-table"></span>
        <span><strong>Tables</strong> Fabriquées en france</span>
        <span class="icon-right_arrow"></span>
      </a> 
    </li>
  </ul>

</section>

{include file="$tpl_dir./bloc-promo-newsletter.tpl"}

{* BLOC NOS PRODUITS EN STOCKS *}

<section id="products-stock">

  <h2> Nos produits en stock <small>livrés chez vous sous 48h / 72h</small></h2>

  <div id="products-stock-swiper">
    <div class="swiper-wrapper">

      <div class="product product-hover swiper-slide"> 

        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
          <div class="promo"> <img src="{$img_dir}compressed/produit-info-20pc.png" /> </div>
        </div>

        <p class="product-title">
          <a class="extend-click-a" href="#">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE</a>
        </p>
        <p class="product-price">
          <span class="price-old">149<sup>€00</sup></span>
          <span class="price-new">119<sup>€00</sup></span>
          <span class="product-tag en-stock">en stock</span>
        </p>

        <div class="product-alt-colors">
          <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
        </div>

        <ul class="product-color-selector">
        <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
        <li>
          <ul class="product-color-selector-submenu">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
          </ul>
        </li>
      </ul>

      </div>

      <div class="product product-hover swiper-slide"> 

        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
          <div class="promo"> <img src="{$img_dir}compressed/produit-info-20pc.png" /> </div>
        </div>

        <p class="product-title">
          <a class="extend-click-a" href="#">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE</a>
        </p>
        <p class="product-price">
          <span class="price">119<sup>€00</sup></span>
          <span class="product-tag en-stock">en stock</span>
        </p>

        <div class="product-alt-colors">
          <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
        </div>

        <ul class="product-color-selector">
        <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
        <li>
          <ul class="product-color-selector-submenu">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
          </ul>
        </li>
      </ul>

      </div>

      <div class="product product-hover swiper-slide"> 

        <div class="product-img">
          <img src="{$img_dir}compressed/illu-chaise.jpg" />
          <div class="promo"> <img src="{$img_dir}compressed/produit-info-20pc.png" /> </div>
        </div>

        <p class="product-title">
          <a class="extend-click-a" href="#">CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE</a>
        </p>
        <p class="product-price">
          <span class="price-old">149<sup>€00</sup></span>
          <span class="price-new">119<sup>€00</sup></span>
          <span class="product-tag en-stock">en stock</span>
        </p>

        <div class="product-alt-colors">
          <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
        </div>

        <ul class="product-color-selector">
        <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
        <li>
          <ul class="product-color-selector-submenu">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
          </ul>
        </li>
      </ul>

      </div>

    </div>

    <div class="swiper-navigation-right">
      <div class="swiper-navigation-container">
        <span class="icon-right_arrow"></span>
      </div>
    </div>

  </div>

  <div id="products-stock-more">
    <span class="span-title"> plus de Produits en stock <small>livrés chez vous sous 48h / 72h</small></span>

    <ul>
      <li> 
        <a href="#">
          <span class="icon-chaises"></span>
          <span><strong>Chaises</strong> en stock</span>
          <span class="icon-right_arrow"></span>
        </a> 
      </li>
      <li> 
        <a href="#">
          <span class="icon-tabouret"></span>
          <span><strong>Tabourets</strong> en stock</span>
          <span class="icon-right_arrow"></span>
        </a> 

      </li>
      <li> 
        <a href="#">
          <span class="icon-table"></span>
          <span><strong>Tables</strong> en stock</span>
          <span class="icon-right_arrow"></span>
        </a> 
      </li>
    </ul>
  </div>

  <div class="clearfix"></div>

</section>

{* BLOC STORELOCATOR *}

<section class="storelocator">

  <div class="storelocator-select">
    <span class="span-title"> 4pieds <br/> un réseau de 24 <br/> magasins partout en <br/> france </span>
    <select>
      <option>Choisissez un magasin</option>
      <option>Paris</option>
      <option>Rennes</option>
    </select>

    <hr />

  </div>

  <div class="storelocator-infos">

    <span class="storelocator-city">Rennes</span>
    <p class=storelocator-adress>
      245 Route de Saint-Malo<br/>
      35000 Rennes
    </p>
    <p class="storelocator-hours">
      OUVERT<br/>
      <small>09:30 - 12:30</small> <br/>
      <small>14:00 - 19:00</small>
    </p>
    <div class="storelocator-opening">
      ouverture exceptionnelle
      lundi de pâques 10:00 - 18:00 
    </div>
    <span class="storelocator-phone">02 52 88 25 06</span>
    <a href="#">découvrez le magasin</a>
  </div>

  <div id="storelocator-map-container">
    <a href="#" id="storelocator-map-fullscreen"><span class="sprite-map"></span></a>
    <div id="storelocator-map"></div>
  </div>

</section>

{* BLOC PROMOS + EXCERPT BLOG *}

<section class="square-blocks-container" id="block-promo-second" >

  <div id="block-blog" class="rectangle-block">
    <span class="span-title"> Nettoyer un meuble en bois: nos astuces pour l’entretien du bois </span>
    <a class="extend-click-a" href="#">Lire la suite sur notre blog design</a>
    <p>
      Comment nettoyer un meuble en bois ? Voilà une question que l’on est souvent amené à se poser. Soumis aux risques du quotidien : poussières, salissures...
    </p>
  </div>

  <div class="square-block no-padding">
    <a href="#">
      <img src="{$img_dir}compressed/bloc-promo2.jpg" />
    </a>
  </div>

  <div id="block-contact" class="square-block">
    <span class="span-title">Contactez-nous</span>
    <ul>
      <li>
        <span class="sprite-operator-blue">&nbsp;</span>
        <p>
          02 99 05 37 00
          <span class="contact-infos">Appel non surtaxé</span>
          <span class="contact-hours">Du lundi au vendredi de 8h à 19h</span>
        </p>
      </li>
      <li>
        <span class="sprite-mail-blue">&nbsp;</span>
        <p>
          Ecrivez-nous
        </p>
      </li>
    </ul>

    <hr />

    <div id="contact-social">
      <h4>Rejoignez-nous</h4>
      <div class="contact-social-list">
        <a href="#"> <span class="sprite-social sprite-social-twitter"></span> </a>
        <a href="#"> <span class="sprite-social sprite-social-pinterest"></span> </a>
        <a href="#"> <span class="sprite-social sprite-social-instagram"></span> </a>
        <a href="#"> <span class="sprite-social sprite-social-facebook"></span> </a>
      </div>

    </div>

  </div>

</section>

{include file="$tpl_dir./bloc-engagement.tpl"}

<section id="welcome">
  <h2>LA RÉFÉRENCE POUR VOS ACHATS DE TABLES, CHAISES ET TABOURETS </h2>
  <input type="checkbox" id="toggler">
  <div class="more">
    <p>
      Bienvenue sur le site 4 pieds, le spécialiste de la vente de chaises, tables et tabourets. 
      Depuis plus de trente ans, nous avons eu l'occasion de mettre à l'oeuvre notre expertise et notre connaissance des produits. 
      Partis de l’ouverture d’un premier magasin en 1982 à Rennes, 4 Pieds compte aujourd’hui 24 magasins dans toute la France. Nos équipes sont composées de professionnels de l’ameublement, 
      qui vous accompagnent dans le choix de vos meubles.
    </p>
    <p>Nunc congue aliquam metus, eget faucibus lacus tincidunt a. Nunc magna dolor, blandit non ultrices ac, suscipit ut erat. Donec congue porta neque, sed mattis felis varius sit amet. Curabitur lobortis eros ac erat vehicula, ut elementum diam sollicitudin. Phasellus consectetur tellus eget neque finibus, eget cursus mi consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Praesent feugiat condimentum justo, non placerat dolor sodales vitae. Morbi in egestas risus. Suspendisse pellentesque sit amet metus ut luctus. Quisque ac tortor quis dui vehicula pretium ut nec dolor. Etiam aliquet metus quam, et placerat elit viverra vitae. Ut interdum condimentum arcu pretium vehicula.</p>
    <p>Fusce nec sagittis dui. Mauris varius magna vel elit aliquam, non aliquet justo consectetur. Praesent lobortis, libero sit amet pellentesque maximus, turpis diam luctus est, id dignissim neque metus lacinia sapien. Nam laoreet, tortor a gravida aliquet, est diam elementum ex, in facilisis orci augue et turpis. Phasellus vel dapibus ipsum, volutpat volutpat nulla. Pellentesque at convallis nibh. Sed ac consectetur lorem. Phasellus vitae eros vitae neque lacinia malesuada. Maecenas nec mollis tortor. Morbi consectetur nisl vel ante volutpat fringilla.</p>
    <label for="toggler" class="toggler-css"></label>
  </div>

</section>