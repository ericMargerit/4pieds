$(document).ready(function (e) {

  // definition des swipers

  var swiper = new Swiper('.slider', {
    pagination: '.swiper-pagination',
    spaceBetween: 0,
    /*autoplay: 2500,
    autoplayDisableOnInteraction: true,*/
    paginationClickable: true,
    centeredSlides: true
  });

  var swiper2 = new Swiper('#top-products, #products-stock-swiper, .product-also-seen', {
    spaceBetween: 18,
    slidesPerView: "auto",
    nextButton: ".swiper-navigation-right"
  });

  var swiper7 = new Swiper('.category-list-swiper-container', {
    slidesPerView: 6,
    direction: 'vertical',
    nextButton: '.swiper-category-next',
    prevButton: '.swiper-category-prev'
  });

  if (screen_size !== "large") { // Si on est pas en large display
    var swiper3 = new Swiper('#product-gallery', {
      freeMode: true,
      spaceBetween: 18,
      slidesPerView: "auto",
      nextButton: ".swiper-navigation-right"
    });
  }

  if (screen_size === "mobile") { // Si on est sur mobile
    var swiper4 = new Swiper('#navigation-account', {
      freeMode: true,
      slidesPerView: 4,
      centeredSlides: false
    });

    var swiper5 = new Swiper('#breadcrumb-order', {
      freeMode: true,
      slidesPerView: "auto",
      centeredSlides: false,
      spaceBetween: 18,
      initialSlide: $('#breadcrumb-order li.active').index()
    });

    var swiper6 = new Swiper('#add-cart-popin-bottom', {
      freeMode: true,
      spaceBetween: 20,
      slidesPerView: 2,
      nextButton: ".swiper-navigation-right"
    });

  }

});
