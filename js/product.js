$(document).ready(function () {

  // Apparition de la barre de partage sociale
  $('#product-share-left').click(function(e){
    e.preventDefault();
    $('#product-share').css('width','auto').css('height','auto');
  });

  // On étend le clic à la div entière ( reco SEO )
  $('.product, #block-blog').click(function(e){

    var target = $(e.target);
    // Si on a cliqué sur un élement du selecteur de couleur on ne lance pas le clic
    if( target.parents('.product-color-selector').length > 0){
      return false;
    }
    e.preventDefault();
    window.location = $(this).find('.extend-click-a').prop('href');
  });

  // Apparition du menu de selection des couleurs pour un produit
  $('.product-personalize-custom-select').click(function (e) {
    e.preventDefault();
    var parentZone = $(this).parent('.product-personalize-zone'),
        parentZoneAll = $('.product-personalize-zone'),
        colorZone = parentZone.find('.product-personalize-selection'),
        colorZoneAll = $('.product-personalize-selection');

    $('.product-personalize-custom-select.active').removeClass('active');
    $(this).addClass('active');
    parentZoneAll.removeClass('active');
    parentZone.addClass('active');
    if (screen_size !== "large") {
      colorZoneAll.css({'right':'-100%'});
      colorZone.css({'right':'0'});
    } else {
      $("#product-infos").find('>div:not(.product-personalize,.product-personalize-selection), .product-personalize .product-personalize-title, .product-personalize span.customSelect, .product-personalize .product-personalize-custom-select:not(.active), >form').css('opacity', 0.5);
      colorZone.css({'display':'block'});
    }
    colorZone.removeClass('choised');
  });

  // Choix couleur active ( Page product menu de couleur visible )
  $('.product-personalize-select-button').click(function (e) {
    e.preventDefault();
    // TODO : Enregistrer couleur active

    // Modification de la couleur dans le faux select
    var data_color = $('.product-personalize-selected img').data('color');
    $('.product-personalize-custom-select.active .product-select-color-style ').removeClass(function (index, css) {
      return (css.match (/(^|\s)product-select-color\d+/g) || []).join(' ');
    }).addClass('product-select-color'+data_color);

    // Modification de la valeur sélectioné dans le select caché
    $('.product-personalize-custom-select.active').parent().find('.product-personalize-select option[value="'+data_color+'"]').prop('selected',true);


    $(this).parent().parent().find('.close').trigger('click');
  });

  // Choix couleur à activer ( Page Product menu de couleur visible )
  $('.product-personalize-list .product-select-color-style').click(function(e) {
    e.preventDefault();
    $('.product-personalize-list .product-select-color-style').removeClass('product-select-color-selected');
    var img_src    = $('.product-personalize-selected img').prop('src');
    var data_color = $(this).data('color');
    var new_img    = "";
    img_src        = img_src.split('/');
    // On parse le nom du fichier pour changer juste le nom de l'image
    for(var i=0; i<(img_src.length-1); i++){
      new_img += img_src[i]+"/";
    }
    new_img += data_color+".png";
    $('.product-personalize-selected img').prop('src',new_img).data('color',data_color);
    $(this).addClass('product-select-color-selected');

    $(this).closest('.product-personalize-selection').addClass('choised');
  });

});
