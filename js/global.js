var screen_size = 'mobile';
var hover_event = false;
var click_event = false;

$(document).ready(function () {

  detectScreenSize();

  $(window).resize(function(){
    detectScreenSize();
  });

  // Apparition du menu mobile
  $('.btn-menu').click(function (e) {
    e.preventDefault();

    var paddingTop = 0;//$(window).scrollTop();
    var stickySize = $('#sticky-menu').outerHeight();
    var windowsSize = $(window).height();
    if ($('#sticky-menu').is(':visible')) {
      paddingTop += stickySize;
      windowsSize -= stickySize;
    }

    $('#menu-float').css('left', 0).css('top', paddingTop + 'px').css('min-height', windowsSize + 'px').mCustomScrollbar("update");
    $('body').addClass('no-scroll');
    $('#overlay').fadeIn();
  });

  // Gestion de la fermeture des popins
  $('.close').click(function (e) {
    e.preventDefault();
    var overlay = $('#overlay');
    var parent = $(this).parent();
    if (parent.hasClass('mCSB_container')) { // Fix à cause de la custom scroll bar qui rajoute des parents
      parent = parent.parent().parent();
      $(parent).mCustomScrollbar("disable");
    }
    $('body').removeClass('no-scroll');

    if (parent.is('#add-cart-popin') || parent.is('#fullscreen-map-container')) {
      parent.fadeOut();
    } else if (parent.hasClass('product-personalize-selection')) {
      $('.product-personalize-custom-select').removeClass('active');
      if (screen_size === "large") {
        parent.hide();
        $("#product-infos").find('>div:not(.product-personalize,.product-personalize-selection), .product-personalize .product-personalize-title, .product-personalize span.customSelect, .product-personalize .product-personalize-custom-select:not(.active), >form').css('opacity', 1);
      } else {
        parent.css('right', '-100%');
      }
    } else if( parent.hasClass('sticky-bar-bubble' )) {
      parent.hide();
      parent.parent().removeClass('active');
    } else {
      parent.css('left', '-110%');
    }
    click_event = false;
    overlay.fadeOut();
  });

  // si on clique sur l'overlay : trigger le clic de fermeture(quelque soit le menu)
  $('#overlay').click(function (e) {
    e.preventDefault();
    $(this).fadeOut();
    $('#menu-float-desktop').css('top','-100%');
  });

  // Apparition sous menu dans le menu mobile
  $('.btn-submenu').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    var submenu = $this.parent().find('.submenu');
    if (submenu.hasClass('active')) {
      $this.find('.icon-minus').addClass('icon-plus').removeClass('icon-minus');
      submenu.removeClass('active');
    } else {
      $this.find('.icon-plus').removeClass('icon-plus').addClass('icon-minus');
      submenu.addClass('active');
    }
  });

  // Apparition des menus ( tablette/desktop ) au clic
  $('#menu a, #desktop-menu a, #desktop-sidelinks a').click(function (e) {
    e.preventDefault();
    if(!hover_event){
      click_event = true;
      showMenu($(this));
    }
  });

  if(screen_size === "large"){
    // Apparition des menus ( tablette/desktop ) en hover
    $('#menu a, #desktop-menu a, #desktop-sidelinks a').hoverIntent({
        sensitivity: 3, // number = sensitivity threshold (must be 1 or higher)
        interval: 500, // number = milliseconds for onMouseOver polling interval
        timeout: 500, // number = milliseconds delay before onMouseOut
        over: function(){
          if(!click_event){
            hover_event = true;
            showMenu($(this));
          }
        },
        out: function(){hover_event = false;} // function = onMouseOut callback (REQUIRED)
    });
  }

  $('.quantity-selector').change(function(e){
    var $this = $(this);
    if($this.val() === '+'){
      $this.replaceWith('<input type="number" value="5" min="0" max="500" class="quantity-selector" />');
      $this.off('change');
    }
  });

  // Apparition menu "Affiner Par" dans la page catégorie ( mobile )
  $('.category-sort').click(function (e) {
    if (screen_size === "mobile") {
      e.preventDefault();
      var menu        = $('#category-sidemenu');
      var overlay     = $('#overlay');
      var paddingTop  = 0;
      var stickySize  = $('#sticky-menu').outerHeight();
      var windowsSize = $(window).height();
      if ($('#sticky-menu').is(':visible')) {
        paddingTop += stickySize;
        windowsSize -= stickySize;
      }
      menu.css('left', '0').css('top', paddingTop + 'px').css('position', 'fixed').css('min-height', windowsSize + 'px').mCustomScrollbar("update");
      $('body').addClass('no-scroll');
      overlay.fadeIn();
    }
  });

  // Gestion des checkbox du menu filter
  $('.category-sidemenu-filter, .category-sidemenu-checkbox').click(function (e) {
    var $this = $(this);
    if ($this.hasClass('category-sidemenu-filter')) {
      toggleCheckbox($this.parent().find('input[type="checkbox"]'));
    } else if ($this.hasClass('category-sidemenu-checkbox')) {
      toggleCheckbox($this.find('input[type="checkbox"]'));
    }
  });


  $('#my-informations-checkbox, .identification-checkbox, .registration-checkbox, .my-address-invoice-checkbox, .payment-checkbox').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    toggleCheckbox($this.find('input[type="checkbox"]'));
    if ($this.hasClass('identification-checkbox')) {
      $('#identification-password').toggle();
      $('#identification-register').toggle();
    } else if ($this.hasClass('registration-delivery-address')) {
      $('#registration-delivery-address').toggle();
    } else if ($this.hasClass('my-address-invoice-address')) {
      $('#my-address-invoice-add').toggle();
    }
  });

  // Fix pour que les labels déclenchent les actions des checkbox associées (checkbox custom)
  $('label:not(.toggler-css)').click(function (e) {
    e.preventDefault();
    var id = $(this).prop('for');
    if (id !== undefined) {
      $("#" + id).parent().trigger('click');
    }
  });

  // Gestion des radios button des pages "Mon profil" & "Inscription"
  $('.my-informations-radio, .registration-radio, .delivery-radio, .payment-radio, .my-addresses-radio').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    var thisClass = $this.prop('class');
    $('.' + thisClass + ' input[type="radio"]').prop('checked', false).parent().removeClass('checked');
    $this.find('input[type="radio"]').prop('checked', true).parent().addClass('checked');

    // Opacité plus faible sur les elements non selectionnés + affichage de la box plus d'infos
    if ($this.hasClass('delivery-radio')) {
      $('.delivery-more-infos').hide();
      $('#delivery-form li').css('opacity', 0.7);
      $this.parent().css('opacity', 1);
      $this.parent().find('.delivery-more-infos').show();
    } else if ($this.hasClass('payment-radio')) {
      $('.payment-more-infos').hide();
      $('#payment-form li').css('opacity', 0.7);
      $this.parent().css('opacity', 1);
      $this.parent().find('.payment-more-infos').show();
    } else if ($this.hasClass('my-addresses-radio')) {
      $('#my-addresses-list li').css('opacity', 0.7);
      $this.parent().css('opacity', 1);
    }

  });

  // Apparitions formulaire "Mon profil"
  $('.title-dropdown').click(function (e) {
    e.preventDefault();

    var content = $(this).parent().find('.content-dropdown');
    if (content.is(':visible')) {
      $(this).find('.icon-down_arrow').removeClass('icon-down_arrow').addClass('icon-right_arrow');
    } else {
      $(this).find('.icon-right_arrow').removeClass('icon-right_arrow').addClass('icon-down_arrow');
    }
    $(this).parent().find('.content-dropdown').toggle();
  });

  // Affichage sous menu, page catégorie, dans le menu des filtres
  $('.category-sidemenu-filter-title').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    var parent = $this.parent();

    var icon = parent.find('.icon');
    icon.toggleClass('icon-right_arrow');
    icon.toggleClass('icon-down_arrow');
    parent.toggleClass('active');
  });

  // Page Mes Commandes
  $('.order-date').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    $this.find('.order-details').toggle();
    $this.find('.icon').toggleClass('icon-right_arrow').toggleClass('icon-down_arrow');
  });

  // Popin Ajout panier
  $('.product-add-cart').click(function (e) {
    e.preventDefault();
    $('#add-cart-popin').css('top', ($(window).scrollTop() + 100) + 'px').fadeIn();
    $('#overlay').fadeIn();
  });

  // Filtre couleur page listing produit ------------
  $('.category-sidemenu-checkbox-color').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    var checkbox = $this.find('input[type="checkbox"]');
    var checked = checkbox.prop('checked');
    if (checked) {
      $this.removeClass('checkbox-color-checked');
      checkbox.prop('checked', false);
    } else {
      $this.addClass('checkbox-color-checked');
      checkbox.prop('checked', true);
    }
  });

  // Scroll custom ----------
  $('.product-personalize-list, .category-sidemenu-submenu-colors, #menu-float').mCustomScrollbar({
    scrollbarPosition: "outside",
    autoHideScrollbar : true
  });

  if(screen_size === "mobile"){
    $('#category-sidemenu').mCustomScrollbar({
      scrollbarPosition: "outside"
    });
    $('#cookie-popin').mCustomScrollbar({
      scrollbarPosition: "inside",
      autoHideScrollbar : true
    });
  }

  // Select custom
  $('.customSelect').customSelect();

  $('.infobulle-container').click(function(e){
    e.preventDefault();
    $('.infobulle-content').toggleClass('active');
  });

  // Sticky elements -----------
  //Check au scroll de l'affichage de la sticky bar
  $(window).scroll(function () {
    stickyBars(this);
  });

  // Check au chargement ( ancre ou refresh en milieu de page par ex. )
  stickyBars(window);

  // Ouverture infobull sticky "Aide"
  $('.open-bubble').click(function(e){
    e.preventDefault();
    $(this).parent().addClass('active').find('.sticky-bar-bubble').show();
  });

  // Maps ----------------
  if ($('#storelocator-map').length > 0) {
    initMap('storelocator-map', 5, 46.00, 2.00, true);
    initMap('menu-desktop-storelocator-map',8, 46.00, 2.00, true);
  }

  $('#storelocator-map-fullscreen').click(function(e){
    e.preventDefault();
    $('#overlay').fadeIn();
    $('#fullscreen-map-container').show();
    $('body').addClass('no-scroll');
    initMap('fullscreen-map', 5, 46.00, 2.00, false);
  });


  // Selections couleurs sur produit
  // Sur mobile/tablette, la liste qui apparait sur desktop devient un pseudo-select
  $('.product-color-selector').click(function(e){
    if(screen !== "large") {
      var $this = $(this);
      e.preventDefault();
      $this.find('.product-color-selector-submenu').toggle();
      if($this.is(':visible')){
        $this.find('a:not(.bound)').addClass('bound').on('click',function(e){
          window.location = $(e.target).parent().prop('href');
        });
        // Si on clique dans le vide cache le sous menu
        $('body:not(.product-color-selector)').one('click',function(){ $('.product-color-selector-submenu:visible').hide(); });
      }
    }
  });
});

function initMap(elem_id, zoom, map_lat, map_lng, disable_ui) {

  // Specify features and elements to define styles.
  var styleArray = [
    {"featureType": "administrative", "elementType": "labels", "stylers": [{"visibility": "off"}]},
    {"featureType": "administrative.country", "elementType": "geometry.stroke", "stylers": [{"visibility": "off"}]},
    {"featureType": "administrative.province", "elementType": "geometry.stroke", "stylers": [{"visibility": "off"}]},
    {"featureType": "landscape", "elementType": "geometry", "stylers": [{"visibility": "on"}, {"color": "#b8b8b8"}]},
    {"featureType": "landscape.natural", "elementType": "labels", "stylers": [{"visibility": "off"}]},
    {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]},
    {"featureType": "road", "elementType": "all", "stylers": [{"color": "#cccccc"}]},
    {"featureType": "road", "elementType": "labels", "stylers": [{"visibility": "off"}]},
    {"featureType": "transit", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]},
    {"featureType": "transit.line", "elementType": "geometry", "stylers": [{"visibility": "off"}]},
    {"featureType": "transit.line", "elementType": "labels.text", "stylers": [{"visibility": "off"}]},
    {"featureType": "transit.station.airport", "elementType": "geometry", "stylers": [{"visibility": "off"}]},
    {"featureType": "transit.station.airport", "elementType": "labels", "stylers": [{"visibility": "off"}]},
    {"featureType": "water", "elementType": "geometry", "stylers": [{"color": "#e8eae5"}]},
    {"featureType": "water", "elementType": "labels", "stylers": [{"visibility": "off"}]}
  ];

  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById(elem_id), {
    center: {lat: map_lat, lng: map_lng},
    scrollwheel: false,
    // Apply the map style array to the map.
    styles: styleArray,
    zoom: zoom,
    disableDefaultUI: disable_ui
  });

  var image = '/themes/4pieds/img/poi.png';
  var beachMarker = new google.maps.Marker({
    position: {lat: 46.00, lng: 2.00},
    map: map,
    icon: image
  });

}

// Gestion des checkbox --
// La checkbox est caché pour être stylisé en css, on doit donc redefinir les actions au clic.
// Format checkbox custom 4pieds :
//
//      <span class="checkbox_custom_class"> <input type="checkbox" id="my_id" /> </span> <label for="my_id"> Label </label>
//
//
function toggleCheckbox(element) {
  var checked = element.prop('checked');
  if (checked === true) {
    element.prop('checked', false);
    element.parent().removeClass('checked');
  } else {
    element.prop('checked', true);
    element.parent().addClass('checked');
  }
}

function stickyBars(element) {

  var headerPos = 200;
  if ($('#responsive-stylesheet').css('z-index') === "1001") {
    headerPos = 161;
  }

  if ($(element).scrollTop() > headerPos) {
    $('#sticky-menu').fadeIn();
  } else {
    $('#sticky-menu').fadeOut();
  }
}

function showMenu(elem){
    var menu           = $('#menu-float-desktop');
    var section        = elem.data('menu');
    var active_section = menu.find('>div:visible').index();
    var new_section    = $('#menu-float-desktop-' + section).index();

    var paddingTop = $('#white-header').outerHeight() + $('header').outerHeight();

    if(screen_size !== "mobile"){
      $("#menu .active, #desktop-menu .active").removeClass('active');
      elem.parent().addClass('active');
    }

    $('#menu-float-desktop > div').hide();
    $('#menu-float-desktop-' + section).show();

    if (section === "account") {
      menu.css('background-color', "transparent");
    } else {
      menu.css('background-color', "#fff");
    }

    if (menu.css('top').substr(0, 1) === "-") {
      menu.css('top', paddingTop + 'px');
      $('#overlay').fadeIn();
    } else if (active_section !== new_section) {

    } else {
      menu.css('top', '-100%');
      //$('#overlay').fadeOut();
    }
}

function detectScreenSize(){
  // Detection de l'affichage et instanciation variable screen pour gérer le responsive
  if ($('#responsive-stylesheet').css('z-index') === "1001") {
    screen_size = 'mobile';
  }else if ($('#responsive-stylesheet').css('z-index') === "1003") {
    screen_size = 'large';
  } else if ($('#responsive-stylesheet').css('z-index') === "1002") {
    screen_size = 'tablet';
  }
}
