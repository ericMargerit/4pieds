{if $page_name === "product" }
<nav id="breadcrumb" class="breadcrumb-4pieds">
  <ul>
    <li><a href="#">Chaises</a></li>
    <li><a href="#">Chaise Plexiglas</a></li>
    <li><h1>CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE ÉPOQUE</h1><span id="product-reference">ref : belle époque</span></li>
  </ul>
</nav>
{/if}

{if $page_name === "order"}
<nav id="breadcrumb-order" class="breadcrumb-4pieds">
  <ul class="swiper-wrapper">
    <li class="swiper-slide"><a href="#">Accueil</a></li>
    <li class="swiper-slide {if $smarty.get.page == 1}active{/if}"><a href="#" >Panier</a></li>
    <li class="swiper-slide {if $smarty.get.page == 2 || $smarty.get.page == 3 }active{/if}"><a href="#" >Adresse</a></li>
    <li class="swiper-slide {if $smarty.get.page == 4}active{/if}"><a href="#" >Livraison</a></li>
    <li class="swiper-slide {if $smarty.get.page == 5 || $smarty.get.page == 6}active{/if}"><a href="#" >Paiement</a></li>
  </ul>
</nav>
{/if}

{if $page_name === "my-account" || $page_name === "history"}
  <nav id="breadcrumb" class="breadcrumb-4pieds">
    <ul>
      <li><a href="#">Accueil</a></li>
      <li><a href="#">Mon Compte</a></li>
    </ul>
  </nav>
{/if}