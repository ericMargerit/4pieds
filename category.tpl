<section id="category-wrapper">

  <!-- En-tête -->
  <div id="category-illustration">
    <h1>Chaises Design</h1>
    <!-- <ul>
      <li><a href="#">Thématique de chaise design</a></li>
      <li><a href="#">Chaises design en plastique</a></li>
      <li><a href="#">Chaise design en bois</a></li>
      <li><a href="#">Chaises design tissus</a></li>
    </ul> -->

    <div class="category-list-swiper">
      <div class="category-list-swiper-container">
        <ul class="swiper-wrapper">
          <li class="swiper-slide"><a href="#">Thématique de chaise design</a></li>
          <li class="swiper-slide"><a href="#">Chaises design en plastique</a></li>
          <li class="swiper-slide"><a href="#">Chaise design en bois</a></li>
          <li class="swiper-slide"><a href="#">Chaises design tissus</a></li>
          <li class="swiper-slide"><a href="#">Chaises design tissus</a></li>
          <li class="swiper-slide"><a href="#">Chaises design tissus</a></li>
          <li class="swiper-slide"><a href="#">Chaises design tissus</a></li>
          <li class="swiper-slide"><a href="#">Chaises design tissus</a></li>
          <li class="swiper-slide"><a href="#">Chaises design tissus</a></li>
          <li class="swiper-slide"><a href="#">Chaises design tissus</a></li>
          <li class="swiper-slide"><a href="#">Chaises design tissus</a></li>
        </ul>
      </div>
      <div class="swiper-category-next"><i class="icon icon-down_arrow"></i></div>
      <div class="swiper-category-prev"><i class="icon icon-up_arrow"></i></div>
    </div>

  </div>

  <!-- barre latérale gauche des filtres -->
  <aside id="category-sidemenu">

    <span class="icon-close close"></span>

    <span class="span-title no-small">Ma sélection</span> <span class="category-filter-reset no-small"> Tout effacer <span class="icon-close"></span> </span>

    <ul id="category-filters-active" class="no-small">
      <li class="en-stock"> en stock <span class="icon-close"></span> </li>
      <li> bois <span class="icon-close"></span> </li>
      <li> métallique <span class="icon-close"></span> </li>
      <li> accoudoirs <span class="icon-close"></span> </li>
      <li> <div class="category-filter-name">nom trèèèèèèèèès long</div> <span class="icon-close"></span> </li>
    </ul>

    <span class="span-title">Affiner par</span>
    <ul id="category-sidemenu-filters">
      <li class="active">
        <a href="#" class="category-sidemenu-filter-title"><span class="icon icon-down_arrow"></span> <span class="category-sidemenu-label">Disponibilité</span></a>
        <ul class="category-sidemenu-submenu">
          <li>
            <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter en-stock">en stock</button>
            <span class="category-sidemenu-en-stock">Livraison sous 48h / 72h</span>
          </li>
        </ul>
        <div class="clearfix"></div>
      </li>
      <li class="active">
        <a href="#" class="category-sidemenu-filter-title"><span class="icon icon-down_arrow"></span> <span class="category-sidemenu-label">Fabrication</span></a>
        <ul class="category-sidemenu-submenu">
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Française</button> </li>
        </ul>
        <div class="clearfix"></div>
      </li>
      <li class="active">
        <a href="#" class="category-sidemenu-filter-title"><span class="icon icon-down_arrow"></span> <span class="category-sidemenu-label">Assise et/ou dossier</span> </a>
        <ul class="category-sidemenu-submenu">
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Croûte de cuir</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Tissu, coton, autre</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Synthétique, Batyline</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Bois, multipli</button> </li>
        </ul>
        <div class="clearfix"></div>
      </li>
      <li>
        <a href="#" class="category-sidemenu-filter-title"><span class="icon icon-right_arrow"></span> <span class="category-sidemenu-label">Couleurs</span> </a>
        <div class="category-sidemenu-submenu category-sidemenu-submenu-colors-container">
          <ul class="category-sidemenu-submenu category-sidemenu-submenu-colors">
            {for $foo=0 to 30}
              <li> <span class="category-sidemenu-checkbox-color product-select-color{$foo}"> <input type="checkbox" /> </span> </li>
                {/for}
          </ul>
        </div>
        <div class="clearfix"></div>
      </li>
      <li>
        <a href="#" class="category-sidemenu-filter-title"><span class="icon icon-right_arrow"></span> <span class="category-sidemenu-label">Assise et/ou dossier</span> </a>
        <ul class="category-sidemenu-submenu">
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Croûte de cuir</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Tissu, coton, autre</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Synthétique, Batyline</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Bois, multipli</button> </li>
        </ul>
        <div class="clearfix"></div>
      </li>
      <li>
        <a href="#" class="category-sidemenu-filter-title"><span class="icon icon-right_arrow"></span> <span class="category-sidemenu-label">Assise et/ou dossier</span> </a>
        <ul class="category-sidemenu-submenu">
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Croûte de cuir</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Tissu, coton, autre</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Synthétique, Batyline</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Bois, multipli</button> </li>
        </ul>
        <div class="clearfix"></div>
      </li>
      <li>
        <a href="#" class="category-sidemenu-filter-title"><span class="icon icon-right_arrow"></span> <span class="category-sidemenu-label">Assise et/ou dossier</span> </a>
        <ul class="category-sidemenu-submenu">
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Croûte de cuir</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Tissu, coton, autre</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Synthétique, Batyline</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Bois, multipli</button> </li>
        </ul>
        <div class="clearfix"></div>
      </li>
      <li>
        <a href="#" class="category-sidemenu-filter-title"><span class="icon icon-right_arrow"></span> <span class="category-sidemenu-label">Assise et/ou dossier</span> </a>
        <ul class="category-sidemenu-submenu">
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Croûte de cuir</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Tissu, coton, autre</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Synthétique, Batyline</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Bois, multipli</button> </li>
        </ul>
        <div class="clearfix"></div>
      </li>
      <li>
        <a href="#" class="category-sidemenu-filter-title"><span class="icon icon-right_arrow"></span> <span class="category-sidemenu-label">Assise et/ou dossier</span> </a>
        <ul class="category-sidemenu-submenu">
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Croûte de cuir</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Tissu, coton, autre</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Synthétique, Batyline</button> </li>
          <li> <span class="category-sidemenu-checkbox"> <input type="checkbox" /> </span> <button class="category-sidemenu-filter">Bois, multipli</button> </li>
        </ul>
        <div class="clearfix"></div>
      </li>
    </ul>


  </aside>

  <div id="category-content">

    <input type="checkbox" id="toggler">
    <div class="more">
      <p id="category-description">
        Depuis quelques années, la déco design est devenu une tendance incontournable en matière de décoration intérieure.
        Les chaises design vont même jusqu'à remplacer, peu à peu, les chaises plus traditionnelles. Pour répondre à la forte demande des consommateurs, les fabricants font preuve d'originalité et d'audace pour proposer des produits uniques qui s'apparentent bien souvent à de véritables oeuvres d'art.
      </p>
      <p>Nunc congue aliquam metus, eget faucibus lacus tincidunt a. Nunc magna dolor, blandit non ultrices ac, suscipit ut erat. Donec congue porta neque, sed mattis felis varius sit amet. Curabitur lobortis eros ac erat vehicula, ut elementum diam sollicitudin. Phasellus consectetur tellus eget neque finibus, eget cursus mi consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In hac habitasse platea dictumst. Praesent feugiat condimentum justo, non placerat dolor sodales vitae. Morbi in egestas risus. Suspendisse pellentesque sit amet metus ut luctus. Quisque ac tortor quis dui vehicula pretium ut nec dolor. Etiam aliquet metus quam, et placerat elit viverra vitae. Ut interdum condimentum arcu pretium vehicula.</p>
      <p>Fusce nec sagittis dui. Mauris varius magna vel elit aliquam, non aliquet justo consectetur. Praesent lobortis, libero sit amet pellentesque maximus, turpis diam luctus est, id dignissim neque metus lacinia sapien. Nam laoreet, tortor a gravida aliquet, est diam elementum ex, in facilisis orci augue et turpis. Phasellus vel dapibus ipsum, volutpat volutpat nulla. Pellentesque at convallis nibh. Sed ac consectetur lorem. Phasellus vitae eros vitae neque lacinia malesuada. Maecenas nec mollis tortor. Morbi consectetur nisl vel ante volutpat fringilla.</p>
      <label for="toggler" class="toggler-css"></label>
    </div>

    <div id="category-list-product-container">

      <div id="category-top-list-product">
        <span id="category-nb-products"> <strong>65</strong> produits</span>
        <form class="no-small">
          <select>
            <option>Trier Par</option>
          </select>
        </form>

        <button class="category-sort" class="only-small">
          <span class="icon-zoom"></span>
          <span>
            <strong>Affiner</strong>
            En stock, Métallique, Bois...
          </span>
        </button>

      </div>

      <div id="category-list-product">

        <div class="product product-hover">

          <div class="product-img">
            <img src="{$img_dir}compressed/illu-chaise.jpg" />
            <div class="promo"> <img src="{$img_dir}compressed/produit-info-20pc.png" /> </div>
          </div>

          <p class="product-title">
            CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
          </p>
          <p class="product-price">
            <span class="price-old">149<sup>€00</sup></span>
            <span class="price-new">119<sup>€00</sup></span>
            <span class="product-tag en-stock">en stock</span>
          </p>

          <div class="product-alt-colors">
            <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
          </div>

          <ul class="product-color-selector">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li>
              <ul class="product-color-selector-submenu">
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
              </ul>
            </li>
          </ul>

        </div>

        <div class="product product-hover">

          <div class="product-img">
            <img src="{$img_dir}compressed/illu-chaise.jpg" />
            <div class="promo"> <img src="{$img_dir}compressed/produit-info-20pc.png" /> </div>
          </div>

          <p class="product-title">
            CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
          </p>
          <p class="product-price">
            <span class="price-old">149<sup>€00</sup></span>
            <span class="price-new">119<sup>€00</sup></span>
            <span class="product-tag en-stock">en stock</span>
          </p>

          <div class="product-alt-colors">
            <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
          </div>

          <ul class="product-color-selector">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li>
              <ul class="product-color-selector-submenu">
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
              </ul>
            </li>
          </ul>

        </div>

        <div class="product product-hover">

          <div class="product-img">
            <img src="{$img_dir}compressed/illu-chaise.jpg" />
            <div class="promo"> <img src="{$img_dir}compressed/produit-info-20pc.png" /> </div>
          </div>

          <p class="product-title">
            CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
          </p>
          <p class="product-price">
            <span class="price-old">149<sup>€00</sup></span>
            <span class="price-new">119<sup>€00</sup></span>
            <span class="product-tag en-stock">en stock</span>
          </p>

          <div class="product-alt-colors">
            <span class="sprite-personalize"></span> <span class="product-alt-colors-label"><strong>2</strong> variantes</span>
          </div>

          <ul class="product-color-selector">
            <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
            <li>
              <ul class="product-color-selector-submenu">
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color1"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color12"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color24"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color30"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color18"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
                <li class="product-color-selector-content"> <a href="#"> <span class="product-select-color-style product-select-color3"/></span> <span class="product-select-color-label no-large">Polycarbonate</span> </a> </li>
              </ul>
            </li>
          </ul>

        </div>

        <div class="category-product-pagination">
          <ul>
            <li class="active"><a href="#">1</a></li>
            <li class=""><a href="#">2</a></li>
            <li class=""><a href="#">3</a></li>
            <li class=""><a href="#">Suivante</a></li>
          </ul>
        </div>

      </div>

    </div>

  </div>

  <div class="clearfix"></div>

</section>

{include file="$tpl_dir./bloc-engagement.tpl"}
