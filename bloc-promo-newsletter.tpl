<!-- BLOC PROMOS + Newsletter -->

<section class="square-blocks-container" id="block-promo-first">

  <div class="square-block no-padding">
    <a href="#">
      <img src="{$img_dir}compressed/bloc-promo.jpg" />
    </a>
  </div>

  <div id="square-block-newsletter" class="square-block">
    <form>
      <div id="newsletter-intro"> 
        <span id="newsletter-text1">Recevez</span> 
        <span id="newsletter-promo">-15<sup>&euro;</sup></span> 
        <span id="newsletter-text2">à valoir sur votre <br/>1ère commande</span>
      </div>
      <p>
        Découvrez en avant-première nos nouveautés en vous inscrivant à notre newsletter
      </p>
      <span>Mon Magasin</span>
      <select>
        <option>Rennes</option>
      </select>
      <span id="newsletter-inputs"><input type="text" placeholder="mon email" placeholder="email" /><input type="submit" value="m'inscrire" /></span>
    </form>
  </div>

  <div id="square-block-satisfaction" class="square-block">
    <span class="span-title">
      Satisfaction garantie !
    </span>
    <img src="{$img_dir}compressed/widget-avis.png" alt="" />
    <div>
      <span class="sprite-satisfaction-thumb"></span>
      <p>
        <span>Satisfait ou remboursé</span>
        14 jours pour décider
      </p>
    </div>
  </div>

  <div id="square-block-personalize" class="square-block">
    <span class="span-title">vos Produits <br/> personnalisés<br/> en 3 clics</span>
    <ul>
      <li>
        <span class="icon-search"></span>
        <p>
          <strong>CHOISISSEZ</strong>
          parmi plus de 5000 produits
        </p>
      </li>
      <li>
        <span class="icon-metre"></span>
        <p>
          <strong>Personnalisez</strong>
          dimension, couleur, finition...
        </p>
      </li>
      <li>
        <span class="sprite-delivery"></span>
        <p>
          <strong>commandez</strong>
          livraison dans nos 24 magasins ou à domicile
        </p>
      </li>
    </ul>
  </div>

</section>