<nav id="navigation-account">
  <ul class="swiper-wrapper">
    <li class="swiper-slide"><a href="#" {if $smarty.get.page == 1}class="active"{/if}>Mes Envies</a></li>
    <li class="swiper-slide"><a href="#">Mes Commandes</a></li>
    <li class="swiper-slide"><a href="#" {if $smarty.get.page == 2}class="active"{/if}>Mon Profil</a></li>
    <li class="swiper-slide"><a href="#" {if $smarty.get.page == 3}class="active"{/if}>Mes Adresses</a></li>
  </ul>
</nav>

{if $smarty.get.page == 1}
<section id="wishlist">
  <div class="product"> 

    <div class="product-img">
      <img src="{$img_dir}compressed/illu-chaise.jpg" />
    </div>

    <p class="product-title">
      CHAISE DESIGN EN POLYCARBONATE 
    </p>
    <p class="product-price">
      <span class="price">119<sup>€00</sup></span>
    </p>

    <div class="wishlist-details">

      <p>
        <span class="wish-product-color"> 
          <span class="product-select-color-style product-select-color1"/></span> Noir
        </span>
        Structure : métal chromé<br/>
        Hauteur avec dossier : 80 cm<br/>
        Profondeur : 50 cm<br/>
        Largeur : 60 cm
      </p>

      <form>
        <fieldset>
          <select>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
          </select>
        </fieldset>

        <button class="product-add-cart"><span class="icon-cart"></span><span class="product-label-add-cart">Commander</span></button>

      </form>

    </div>

  </div>

  <div class="product"> 

    <div class="product-img">
      <img src="{$img_dir}compressed/illu-chaise.jpg" />
    </div>

    <p class="product-title">
      CHAISE DESIGN EN POLYCARBONATE OPAQUE STYLE RÉGENCE BELLE
    </p>
    <p class="product-price">
      <span class="price-old">149<sup>€00</sup></span>
      <span class="price-new">119<sup>€00</sup></span>
    </p>

    <div class="wishlist-details">

      <p>
        <span class="wish-product-color"> 
          <span class="product-select-color-style product-select-color4"/></span> Noir
        </span>
        Structure : métal chromé<br/>
        Hauteur avec dossier : 80 cm<br/>
        Profondeur : 50 cm<br/>
        Largeur : 60 cm
      </p>

      <form>
        <fieldset>
          <select>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
          </select>
        </fieldset>

        <button class="product-add-cart"><span class="icon-cart"></span><span class="product-label-add-cart">Commander</span></button>

      </form>

    </div>

  </div>

</section>
{/if}

{if $smarty.get.page == 2}
<section id="my-informations">

  <p>
    M. Benedict Cumberbatch<br/>
    b.cumberbatch@gmail.com<br/>
    12.07.1978
  </p>

  <form>
    <span class="my-informations-dropdown title-dropdown"> <span class="icon-right_arrow"></span> modifier mes informations personnelles </span>
    <fieldset class="content-dropdown">
      <p class="error-message"> Veuillez remplir tous les champs </p>
      <span class="my-informations-radio"><input type="radio" name="id_gender" id="id_gender"></span> <label for="id_gender">M.</label>
      <span class="my-informations-radio"><input type="radio" name="id_gender" id="id_gender2"></span> <label for="id_gender2">Mme</label>
      <span class="my-informations-radio"><input type="radio" name="id_gender" id="id_gender3"></span> <label for="id_gender3">Mlle</label>

      <input type="text" name="" placeholder="Prénom" value="" />
      <input type="text" name="" placeholder="Nom" value="" />
      <input type="text" name="" placeholder="Email" value="" />
      <input type="text" name="" placeholder="Mot de passe actuel" value="" />
      <input type="text" name="" placeholder="Nouveau mot de passe" value="" />
      <input type="text" name="" placeholder="Confirmation mot de passe" value="" />

      <select name="">
        <option>1</option>
        <option>2</option>
        <option>3</option>
      </select>

      <select name="">
        <option>Janvier</option>
        <option>Février</option>
        <option>Mars</option>
      </select>

      <select name="">
        <option>1978</option>
        <option>1979</option>
        <option>1980</option>
      </select>

      <span id="my-informations-checkbox"><input type="checkbox" name="" id="newsletter-checkbox" /></span> <label for="newsletter-checkbox">S'inscrire à la newsletter</label>

      <input type="submit" value="valider mes informations" />

    </fieldset>
  </form>

</section>
{/if}

{if $smarty.get.page == 3}
<section id="my-addresses">

  <ul id="my-addresses-list">
    <li>
      <p class="address">
        <strong>Benedict Cumberbatch</strong>
        <br/>
        13 Place des Lices<br/>
        35000<br/>
        RENNES<br/>
        <br/>
        France
      </p>

      <div class="address-actions">
        <a href="#"> <span class="icon-edit"></span> </a>
        <a href="#"> <span class="icon-trash"></span> </a>
      </div>

      <div class="clearfix"></div>
    </li>

    <li>
      <p class="address">
        <strong>Benedict Cumberbatch</strong>
        <br/>
        13 Place des Lices<br/>
        35000<br/>
        RENNES<br/>
        <br/>
        France
      </p>

      <div class="address-actions">
        <a href="#"> <span class="icon-edit"></span> </a>
        <a href="#"> <span class="icon-trash"></span> </a>
      </div>

      <div class="clearfix"></div>
    </li>

  </ul>

  <form id="my-address-add">

    <span class="title-dropdown"> <span class="icon-right_arrow"></span> ajouter une nouvelle adresse </span> 

    <fieldset class="content-dropdown">
      <input type="text" placeholder="Nom" class="error" />
      <input type="text" placeholder="Prénom" />
      <input type="text" placeholder="Adresse" />
      <input type="text" placeholder="Complément d'Adresse" />
      <input type="text" placeholder="Ville" />
      <input type="text" placeholder="Code Postal" />

      <input type="submit" value="Ajouter l’adresse"/>

    </fieldset>

  </form>

</section>
{/if}